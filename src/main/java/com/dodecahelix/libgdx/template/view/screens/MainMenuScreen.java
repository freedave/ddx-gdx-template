/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.screens;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.types.ScreenType;
import com.dodecahelix.libgdx.template.types.ScreenType.CommonScreen;
import com.dodecahelix.libgdx.template.ui.RgbColor;
import com.dodecahelix.libgdx.template.ui.SkinClass;
import com.dodecahelix.libgdx.template.view.View;

public class MainMenuScreen extends View {

    protected ScreenType mainApplicationScreenType;
    protected Table buttonTable;

    public MainMenuScreen(ControllerMap controllers,
                          Properties options,
                          ScreenType mainApplicationScreenType) {

        super(controllers, options);
        this.mainApplicationScreenType = mainApplicationScreenType;
    }

    public void init() {
        super.init();
    }

    @Override
    public Set<SpaceId> getAssociatedSpaces() {
        // If the screen doesn't care about the model (or wants to pause it), return an empty HashSet
        return new HashSet<SpaceId>();
    }

    public void buildMenu() {

        // change font size and color
        Skin skin = controllers.getResourceController().getSkin();

        // create a table and fill up the screen
        buttonTable = new Table(skin);
        buttonTable.setFillParent(true);
        stage.addActor(buttonTable);

        String welcomeMessage = properties.getStringProperty(PropertyName.WELCOME_MESSAGE.name());
        float spacing = properties.getScaledFloatProperty(PropertyName.UI_STANDARD_PADDING.name()) * 2;

        buttonTable.add(new Label(welcomeMessage, skin.get(SkinClass.XL_LABEL.name(), LabelStyle.class))).spaceBottom(spacing);
        buttonTable.row();

        buildMenuButtons();
    }

    /**
     *   Override this method to add new screen buttons
     *
     */
    public void buildMenuButtons() {
        buildMenuButton("Start", mainApplicationScreenType.getCode());

        // TODO - New Game and Load Game button?
        buildMenuButton("Options", CommonScreen.OPTIONS.getId());
        buildMenuButton("Credits", CommonScreen.CREDITS.getId());
        buildMenuButton("Exit", CommonScreen.EXIT.getId());
    }

    public TextButton buildMenuButton(final String displayName, final int screenId) {

        // button will bring up a new screen
        Skin skin = controllers.getResourceController().getSkin();
        TextButton menuButton = new TextButton(displayName, skin.get(SkinClass.XL_BUTTON.name(), TextButtonStyle.class));

        // set the background color of the button -- too bad this isn't part of the style
        String menuButtonColorString = properties.getStringProperty(PropertyName.UI_BUTTON_BACKGROUND_COLOR.name());
        RgbColor menuButtonColor = RgbColor.valueOf(menuButtonColorString);
        menuButton.setColor(menuButtonColor.getColor());

        menuButton.addListener(new ActorGestureListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                if (CommonScreen.EXIT.getId() == screenId) {
                    Gdx.app.exit();
                }

                controllers.getScreenController().setScreen(screenId);
            }
        });

        float buttonHeight = properties.getScaledFloatProperty(PropertyName.UI_BIG_BUTTON_HEIGHT.name());
        float buttonSpacing = properties.getScaledFloatProperty(PropertyName.UI_STANDARD_PADDING.name());

        buttonTable.add(menuButton).uniform().fillX().height(buttonHeight).spaceBottom(buttonSpacing);
        buttonTable.row();

        return menuButton;
    }

    @Override
    public void render(float delta) {
        stage.act(delta);

        // clear the screen with the given RGB color (black)
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
    }


    @Override
    public void show() {
        // clear the old actors and rebuild the stage
        stage.clear();

        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "Showing MainMenuScreen");
        this.addTiledBackgroundTexture();
        buildMenu();

        // set the stage as the input processor
        Gdx.input.setInputProcessor(stage);
    }

}
