/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.resource;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.control.Controller;

/**
 *
 *   Responsible for caching, retrieval and disposal of media resources, including:
 *   - Textures
 *   - Fonts
 *   - Sounds
 *
 * @author petersd
 *
 */
public interface ResourceController extends Controller {

    public void init(Properties properties);

    /**
     *   Called once, when the splash screen is brought up.  Loads all textures, fonts, sounds, music
     */
    public void loadMedia();

    // Since media is loaded asynchronously, add a test to ensure it has been loaded
    public boolean isMediaLoaded();

    public void disposeAllMediaResources();

    public TextureRegion findTexture(TextureKey textureKey);

    public NinePatch findNinePatch(TextureKey textureKey);

    public TextureRegion findIcon(IconKey iconTextureKey);

    public BitmapFont findFont(FontStyle fontKey, int fontSize);

    public Sound findSound(SoundKey soundKey);

    public Music findMusic(MusicKey musicKey);

    public Skin getSkin();

}
