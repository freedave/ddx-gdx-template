/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view;

import com.badlogic.gdx.utils.IntMap;
import com.dodecahelix.libgdx.template.types.ScreenType.CommonScreen;
import com.dodecahelix.libgdx.template.view.screens.CreditsScreen;
import com.dodecahelix.libgdx.template.view.screens.ExitScreen;
import com.dodecahelix.libgdx.template.view.screens.LoadingScreen;
import com.dodecahelix.libgdx.template.view.screens.MainMenuScreen;
import com.dodecahelix.libgdx.template.view.screens.OptionsScreen;
import com.dodecahelix.libgdx.template.view.screens.SplashScreen;

public class ViewMap {

    MainMenuScreen mainMenuScreen;

    LoadingScreen loadingScreen;

    CreditsScreen creditsScreen;

    SplashScreen splashScreen;

    OptionsScreen optionsScreen;

    ExitScreen exitScreen;

    // maps ScreenTypes (by id) to view
    private IntMap<View> views;

    public ViewMap(MainMenuScreen mainMenuScreen,
                   LoadingScreen loadingScreen,
                   CreditsScreen creditsScreen,
                   SplashScreen splashScreen,
                   OptionsScreen optionsScreen,
                   ExitScreen exitScreen) {

        super();

        this.mainMenuScreen = mainMenuScreen;
        this.loadingScreen = loadingScreen;
        this.creditsScreen = creditsScreen;
        this.splashScreen = splashScreen;
        this.optionsScreen = optionsScreen;
        this.exitScreen = exitScreen;

        views = new IntMap<View>();
    }

    public void initMap() {
        views.put(CommonScreen.MAIN_MENU.getId(), mainMenuScreen);
        views.put(CommonScreen.LOADING.getId(), loadingScreen);
        views.put(CommonScreen.CREDITS.getId(), creditsScreen);
        views.put(CommonScreen.OPTIONS.getId(), optionsScreen);
        views.put(CommonScreen.EXIT.getId(), exitScreen);
        views.put(CommonScreen.SPLASH.getId(), splashScreen);

        // splash screen is a special case
        splashScreen.init();
    }

    public void initViews() {
        for (View view : views.values()) {
            // don't initialize spalshscreen twice
            if (!(view instanceof SplashScreen)) {
                view.init();
            }
        }
    }

    public View getView(int screenType) {
        return views.get(screenType);
    }

    public void add(int screenType, View viewScreen) {
        views.put(screenType, viewScreen);
    }
}
