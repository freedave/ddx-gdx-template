/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.resource;

public class FontStyle {

    // if a font style is added, please add it to the fontStyles array as well
    public static final FontStyle ARIMO = new FontStyle("ttf/arimo.ttf", 1, "Arimo");
    public static final FontStyle ARVO = new FontStyle("ttf/arvo.ttf", 2, "Arvo");
    public static final FontStyle ROSARIO = new FontStyle("ttf/rosario.ttf", 3, "Rosario");
    public static final FontStyle ROBOTO = new FontStyle("ttf/roboto.ttf", 4, "Roboto Slab");
    public static final FontStyle FJORD = new FontStyle("ttf/fjord.ttf", 5, "Fjord One");
    public static final FontStyle OPENSANS = new FontStyle("ttf/opensans.ttf", 6, "Open Sans");
    public static final FontStyle NEUTON = new FontStyle("ttf/neuton.ttf", 7, "Neuton");
    public static final FontStyle GARAMOND = new FontStyle("ttf/ebgaramond.ttf", 8, "E.B. Garamond");

    public static FontStyle[] fontStyles = new FontStyle[]{
    ARIMO, ROSARIO, ROBOTO, FJORD, OPENSANS, ARVO, NEUTON, GARAMOND
    };

    private int lookupId;
    private String resourcePath;
    private String displayName;

    public FontStyle(String resourcePath, int lookupId, String displayName) {
        this.resourcePath = resourcePath;
        this.lookupId = lookupId;
        this.displayName = displayName;

        // require lookup id<1000 in order to generate unique ID keys for lookups
        if (lookupId > 1000) {
            throw new IllegalArgumentException("Lookup id must be less than 1000");
        }
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public int getLookupId() {
        return lookupId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static FontStyle getByName(String name) {
        for (int i = 0; i < fontStyles.length; i++) {
            if (fontStyles[i].displayName.equalsIgnoreCase(name)) {
                return fontStyles[i];
            }
        }
        throw new IllegalArgumentException("No FontStyle for name : " + name);
    }
}
