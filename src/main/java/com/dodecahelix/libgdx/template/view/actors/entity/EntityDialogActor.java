/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors.entity;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.model.interfaces.UiEntity;
import com.dodecahelix.libgdx.template.types.Attribute.StandardAttribute;
import com.dodecahelix.libgdx.template.view.resource.TextureKey;

public class EntityDialogActor extends EntityActor<UiEntity> {

    private boolean isAddedToStage = false;

    /**
     *   Whether to have a transparent background or not
     */
    private boolean transparentFrame = false;

    protected Table dialog;
    protected Image frame;

    @Override
    public void build() {
        dialog = new Table(resources.getSkin());

        TextureKey dialogTexture = properties.getTextureProperty(PropertyName.DIALOG_FRAME_TEXTURE.name());
        NinePatch ninePatchFrame = resources.findNinePatch(dialogTexture);
        frame = new Image(ninePatchFrame);
    }

    public void setTransparentFrame(boolean transparentFrame) {
        this.transparentFrame = transparentFrame;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        if (isInitialized()) {
            super.draw(batch, parentAlpha);

            // has position changed?  if so, move it
            float screenX = getAttributes().getFloatValue(StandardAttribute.POSITION_X.getId(), 0);
            float screenY = getAttributes().getFloatValue(StandardAttribute.POSITION_Y.getId(), 0);

            float viewportWidth = this.getStage().getWidth();
            float viewportHeight = this.getStage().getHeight();

            // convert percents
            if (screenX <= 1 && screenX > 0) {
                screenX = viewportWidth * screenX;
            }
            if (screenY <= 1 && screenY > 0) {
                screenY = viewportHeight * screenY;
            }

            float width = getAttributes().getFloatValue(StandardAttribute.WIDTH.getId(), 0);
            float height = getAttributes().getFloatValue(StandardAttribute.HEIGHT.getId(), 0);
            if (width <= 1 && width > 0) {
                width = viewportWidth * width;
            }
            if (height <= 1 && height > 0) {
                height = viewportHeight * height;
            }

            boolean fillX = getAttributes().getBooleanValue(StandardAttribute.FILL_X.getId(), false);
            if (fillX) {
                width = viewportWidth - screenX * 2;
            }
            boolean fillY = getAttributes().getBooleanValue(StandardAttribute.FILL_Y.getId(), false);
            if (fillY) {
                height = viewportWidth - screenY * 2;
            }

            dialog.setBounds(screenX, screenY, width, height);
            frame.setBounds(screenX, screenY, width, height);

            if (!isAddedToStage) {
                if (!transparentFrame) {
                    this.getStage().addActor(frame);
                }
                this.getStage().addActor(dialog);
                isAddedToStage = true;
            }
        }
    }

    @Override
    public boolean remove() {
        dialog.remove();
        isAddedToStage = false;
        return super.remove();
    }

    @Override
    public void resize(int width, int height) {
        dialog.invalidate();
        frame.invalidate();
    }

}
