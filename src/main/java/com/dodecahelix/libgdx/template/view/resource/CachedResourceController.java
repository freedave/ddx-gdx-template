/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.resource;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.IntMap;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.config.SharedTypes;
import com.dodecahelix.libgdx.template.ui.JSkin;

/**
 *   Implements a cache of all resource files to prevent recreation of the same resource
 *
 */
public class CachedResourceController implements ResourceController {

    private AssetManager assetManager;

    // Cache regions and sound to prevent memory leak when reusing resources
    private IntMap<TextureRegion> textureMap;
    private IntMap<NinePatch> ninePatchMap;
    private IntMap<TextureRegion> iconMap;
    private IntMap<BitmapFont> fontMap;

    private FontStyle defaultFont;

    private JSkin skin;

    public CachedResourceController() {
        textureMap = new IntMap<TextureRegion>();
        iconMap = new IntMap<TextureRegion>();
        fontMap = new IntMap<BitmapFont>();
        ninePatchMap = new IntMap<NinePatch>();
    }

    @Override
    public void init(Properties properties) {
        assetManager = new AssetManager();
        defaultFont = FontStyle.getByName(properties.getStringProperty(PropertyName.UI_DEFAULT_FONT.name()));

        skin = new JSkin(properties, defaultFont);
    }

    @Override
    public void disposeAllMediaResources() {
        assetManager.dispose();

        // fonts aren't part of the asset manager, dispose on its own
        for (BitmapFont font : fontMap.values()) {
            font.dispose();
        }
    }

    @Override
    public void loadMedia() {
        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "Loading media files (asynchronously)");

        // TODO -- Check for null assetManager and throw Exception - method was run before initialization
        assetManager.load(SharedConstants.TEXTURE_ATLAS_LOCATION, TextureAtlas.class);
        assetManager.load(SharedConstants.ICON_ATLAS_LOCATION, TextureAtlas.class);

        Iterator<SoundKey> sounds = SharedTypes.getInstance().getSounds();
        Iterator<MusicKey> music = SharedTypes.getInstance().getMusic();
        while (sounds.hasNext()) {
            SoundKey sound = sounds.next();
            assetManager.load(sound.getResourcePath(), Sound.class);
        }
        while (music.hasNext()) {
            MusicKey song = music.next();
            assetManager.load(song.getResourcePath(), Music.class);
        }
    }

    @Override
    public boolean isMediaLoaded() {
        boolean loaded = true;
        if (assetManager == null || !assetManager.update()) {
            loaded = false;
        }

        // must wait until resources are loaded before skin initialization
        if (loaded && !skin.isInitialized()) {
            skin.init(this);
        }

        return loaded;
    }

    @Override
    public TextureRegion findTexture(TextureKey textureKey) {
        TextureRegion region = textureMap.get(textureKey.getLookupId());
        if (region == null) {
            TextureAtlas atlas = assetManager.get(SharedConstants.TEXTURE_ATLAS_LOCATION, TextureAtlas.class);
            region = atlas.findRegion(textureKey.getResourcePath());
            if (region == null) {
                throw new IllegalStateException("There is no image for resource path : " + textureKey.getResourcePath());
            }
            textureMap.put(textureKey.getLookupId(), region);
        }
        return region;
    }

    @Override
    public NinePatch findNinePatch(TextureKey textureKey) {
        NinePatch ninePatch = ninePatchMap.get(textureKey.getLookupId());
        if (ninePatch == null) {
            ninePatch = new NinePatch(findTexture(textureKey),
            textureKey.getCornerWidth(),
            textureKey.getCornerWidth(),
            textureKey.getCornerWidth(),
            textureKey.getCornerWidth());

            ninePatchMap.put(textureKey.getLookupId(), ninePatch);
        }
        return ninePatch;
    }

    @Override
    public TextureRegion findIcon(IconKey iconKey) {
        TextureRegion region = iconMap.get(iconKey.getLookupId());
        if (region == null && assetManager.isLoaded(SharedConstants.ICON_ATLAS_LOCATION)) {
            TextureAtlas atlas = assetManager.get(SharedConstants.ICON_ATLAS_LOCATION, TextureAtlas.class);
            region = atlas.findRegion(iconKey.getResourcePath());
            iconMap.put(iconKey.getLookupId(), region);
        }
        return region;
    }

    @Override
    public BitmapFont findFont(FontStyle fontStyle, int fontSize) {
        //font is unique to  both fontStyle and fontId (i.e; can have the same font, different size)
        int styleId = fontStyle.getLookupId();
        if (styleId > 1000) {
            throw new IllegalStateException("FontStyle cannot have a lookup id > 1000");
        }
        int fontId = styleId * 1000 + fontSize;

        BitmapFont font = fontMap.get(fontId);
        if (font == null) {
            font = createBitmapFont(fontStyle, (int) fontSize);
            fontMap.put(fontId, font);
        }

        return font;
    }

    @Override
    public Sound findSound(SoundKey soundKey) {
        return assetManager.get(soundKey.getResourcePath(), Sound.class);
    }

    @Override
    public Music findMusic(MusicKey musicKey) {
        return assetManager.get(musicKey.getResourcePath(), Music.class);
    }

    @Override
    public Skin getSkin() {
        return skin;
    }

    private BitmapFont createBitmapFont(FontStyle fontStyle, int size) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(fontStyle.getResourcePath()));

        FreeTypeFontParameter param = new FreeTypeFontParameter();
        param.size = size;

        //BitmapFont font = generator.generateFont(size);
        BitmapFont font = generator.generateFont(param);
        generator.dispose();

        return font;
    }

    @Override
    public String getControllerKey() {
        return "controller.resource";
    }

}
