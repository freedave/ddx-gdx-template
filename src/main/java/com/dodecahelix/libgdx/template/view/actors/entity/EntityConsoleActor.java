/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors.entity;

import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.model.interfaces.ConsoleEntity;
import com.dodecahelix.libgdx.template.text.TextLine;
import com.dodecahelix.libgdx.template.ui.RgbColor;
import com.dodecahelix.libgdx.template.ui.StyledTextTable;
import com.dodecahelix.libgdx.template.view.resource.FontStyle;

/**
 *   Console (dialog) that appears on the screen, with styled text contents that can be appended to.
 *
 */
public class EntityConsoleActor extends EntityDialogActor {

    private StyledTextTable consoleTable;
    private ScrollPane scrollPane;

    private RgbColor defaultFontColor;

    // flag to indicate that the console position needs to be reset
    private boolean consoleDirty = false;

    // pause all text processing until the actor is clicked on
    private boolean paused = false;

    // height of the console table where newly appended text appears
    private float newTextPosition = 0.0f;

    private String fontPropertyName;

    public EntityConsoleActor() {
        this(PropertyName.UI_DEFAULT_FONT.name());
    }

    public EntityConsoleActor(String consoleFontPropertyName) {
        this.fontPropertyName = consoleFontPropertyName;
    }

    @Override
    public void build() {
        super.build();

        String defaultFontStyleName = properties.getStringProperty(fontPropertyName);
        FontStyle defaultFontStyle = FontStyle.getByName(defaultFontStyleName);

        consoleTable = new StyledTextTable(resources, properties, defaultFontStyle);
        scrollPane = new ScrollPane(consoleTable, resources.getSkin());

        String defaultFontColorString = properties.getStringProperty(PropertyName.CONSOLE_COLOR_SYSTEM_FONT.name());
        defaultFontColor = RgbColor.valueOf(defaultFontColorString);

        // erase the background.  The dialog already has a frame
        ScrollPaneStyle style = new ScrollPaneStyle(scrollPane.getStyle());
        style.background = null;
        scrollPane.setStyle(style);

        // don't let the scroll knob stretch
        scrollPane.setVariableSizeKnobs(false);

        // this attaches the console to the stage.  it should be resized to the parent dialog on every frame.
        dialog.addActor(scrollPane);

        // if you click anywhere on the scrollpane, unpause the console
        scrollPane.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EntityConsoleActor.this.paused = false;
            }
        });
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        // text has been appended to the console.  autoscroll down so that we can see the text
        if (consoleDirty) {
            // scroll to the end of the text
            // scrollPane.setScrollY(consoleTable.getHeight());

            // instead of scrolling to the end of the text
            // scroll to the point of where the new text begins
            if (newTextPosition > 0) {
                scrollPane.setScrollY(newTextPosition);
            }
            consoleDirty = false;
        }

        if (isInitialized()) {
            resizeFrame();

            // add a new line, if necessary
            boolean hasNewLines = !entityClone.isSynched();
            if (hasNewLines && !paused) {
                // keep track of the point where the new text begins
                newTextPosition = consoleTable.getHeight();

                TextLine newLine = null;
                Iterator<TextLine> newLines = ((ConsoleEntity) entityClone).getNewLines().iterator();
                while (newLines.hasNext()) {
                    newLine = newLines.next();

                    if (!paused) {
                        appendTextLine(newLine);
                        if (newLine.isPause()) {
                            // pause until clicked
                            paused = true;
                            consoleDirty = true;
                        }
                        newLines.remove();
                    }
                }

                if (((ConsoleEntity) entityClone).getNewLines().isEmpty()) {
                    entityClone.setSynched(true);
                    consoleDirty = true;
                }
            }
        }
    }

    private void resizeFrame() {
        float padding = properties.getScaledFloatProperty(PropertyName.UI_STANDARD_PADDING.name());
        scrollPane.setBounds(padding * 2, padding * 2, dialog.getWidth() - 4 * padding, dialog.getHeight() - 4 * padding);
    }

    private void appendTextLine(TextLine textLine) {
        if (textLine.getColor() == null) {
            textLine.setColor(defaultFontColor);
        }
        consoleTable.addTextLine(textLine);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);

        scrollPane.invalidate();
        consoleTable.invalidate();
    }

}
