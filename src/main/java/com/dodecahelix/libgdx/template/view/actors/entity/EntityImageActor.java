/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.dodecahelix.libgdx.template.model.twodim.GridEntity;
import com.dodecahelix.libgdx.template.view.resource.TextureKey;

public class EntityImageActor extends EntityActor<GridEntity> {

    private TextureKey texture;
    protected TextureRegion textureTile;

    protected int imgHeight;
    protected int imgWidth;

    public EntityImageActor(TextureKey texture) {
        this.texture = texture;
    }

    public void build() {
        this.textureTile = resources.findTexture(texture);

        // set actor width and height
        imgHeight = textureTile.getRegionHeight();
        imgWidth = textureTile.getRegionWidth();
        this.setWidth(imgWidth);
        this.setHeight(imgHeight);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (isInitialized()) {
            float size = this.getEntityWidth();

            Color originalColor = batch.getColor();
            batch.setColor(this.getColor());
            batch.draw(textureTile,
            this.getScreenX() - size / 2,
            this.getScreenY() - size / 2,
            size / 2,
            size / 2,
            size, size,
            1.0f, 1.0f,
            this.getEntityRotation());

            batch.setColor(originalColor);
        }
    }

    @Override
    public void resize(int width, int height) {
    }

}
