/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view;

import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.LongMap;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertiesUtil;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.control.ScreenController;
import com.dodecahelix.libgdx.template.control.events.StandardViewEventType;
import com.dodecahelix.libgdx.template.control.events.ViewEventController;
import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.ui.TiledBackground;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityActor;
import com.dodecahelix.libgdx.template.view.resource.TextureKey;

public abstract class View implements Screen {

    protected Stage stage;

    private Viewport viewport;

    protected ControllerMap controllers;

    protected ViewEventController eventHandler;

    protected Properties properties;

    private Bounds screenBounds;

    public View(ControllerMap controllers, Properties properties) {
        this.controllers = controllers;
        this.properties = properties;

        this.eventHandler = controllers.getActionController();
    }

    /**
     *   Init method is called once per application lifecycle, when the splash screen is brought up
     */
    public void init() {
        viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport);
    }

    public abstract Set<SpaceId> getAssociatedSpaces();

    @Override
    public void show() {
        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "Showing screen : " + this.getClass() + ".  Controllers = " + controllers);
        controllers.getSpaceController().activateSpaces(getAssociatedSpaces());

        // (re)build stage
        stage = controllers.getStageController().build(stage);

        // notify the model that the screen is now showing
        eventHandler.generateEvent(StandardViewEventType.SCREEN_SHOW.getEventTypeId());

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        screenBounds = controllers.getScreenController().getScreenBounds();

        // process actions
        controllers.getActionController().handleActions();

        // update model
        controllers.getBumpController().bump(delta);

        // add new actors, remove destroyed actors
        controllers.getStageController().updateStage(stage);

        // controller should synchronize actor properties with their associated entities
        LongMap<EntityActor<? extends Entity>> actors = controllers.getStageController().getActorMap();
        controllers.getEntityActorSyncController().updateActors(actors, screenBounds);

        stage.act(Gdx.graphics.getDeltaTime());

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
    }

    protected ControllerMap getControllerMap() {
        return controllers;
    }

    @Override
    public void resize(int width, int height) {
        viewport.setWorldHeight(Gdx.graphics.getHeight());
        viewport.setWorldWidth(Gdx.graphics.getWidth());
        stage.setViewport(viewport);

        // rescale properties
        PropertiesUtil.scaleProperties(properties);

        // update the screen bounds (ScreenController will pick up the new width and height - no need to pass in)
        controllers.getScreenController().resize();

        // resize entity actors
        controllers.getStageController().resize(width, height);
    }

    /**
     *   Add the standard tiled background to this screen
     *
     * @return
     */
    protected void addTiledBackgroundTexture() {
        ScreenController screenController = controllers.getScreenController();

        TextureKey backgroundTexture = properties.getTextureProperty(PropertyName.UI_BACKGROUND_TILE.name());
        TextureRegion backgroundTextureRegion = controllers.getResourceController().findTexture(backgroundTexture);
        TiledBackground background = new TiledBackground(screenController.getScreenBounds().getWidth(),
        screenController.getScreenBounds().getHeight(),
        backgroundTextureRegion);

        stage.addActor(background);
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

}
