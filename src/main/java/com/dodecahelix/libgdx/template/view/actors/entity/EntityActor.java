/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors.entity;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.control.events.ViewEventController;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.attributes.AttributeHolder;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;

/**
 *   Base class for an Actor that is linked to a model entity
 *
 * @author dpeters
 *
 */
public abstract class EntityActor<T extends Entity> extends Actor implements Poolable {

    protected ResourceController resources;
    protected ViewEventController eventHandler;
    protected Properties properties;

    // this is a clone of the model entity for read-only operations (write will do nothing)
    protected T entityClone;

    private boolean initialized = false;

    // should be the center of the actor, in the screen coordinate system
    private float screenX;
    private float screenY;

    // represents the size and height of this entity
    private float entityWidth;
    private float entityHeight;

    // represents the angle of rotation (000 being turn north, going clockwise about the center of the actor
    private float entityRotation;

    // holds other attributes
    private AttributeHolder attributes;

    public EntityActor() {
        attributes = new AttributeHolder();
    }

    @Override
    public void reset() {
        initialized = false;

        this.screenX = 0;
        this.screenY = 0;
        this.entityWidth = 0;
        this.entityHeight = 0;
        this.entityRotation = 0;
    }

    public abstract void build();

    public abstract void resize(int width, int height);

    public void initialize(ResourceController resources,
                           Properties options,
                           ViewEventController eventHandler) {

        this.resources = resources;
        this.eventHandler = eventHandler;
        this.properties = options;

        this.initialized = true;
    }

    public T getEntityClone() {
        return entityClone;
    }

    public void setEntityClone(T entityClone) {
        this.entityClone = entityClone;
    }

    public ResourceController getResources() {
        return resources;
    }

    public float getScreenX() {
        return screenX;
    }

    public void setScreenX(float x) {
        this.screenX = x;
    }

    public float getScreenY() {
        return screenY;
    }

    public void setScreenY(float y) {
        this.screenY = y;
    }

    public float getEntityWidth() {
        return entityWidth;
    }

    public void setEntityWidth(float entityWidth) {
        this.entityWidth = entityWidth;
    }

    public float getEntityHeight() {
        return entityHeight;
    }

    public void setEntityHeight(float entityHeight) {
        this.entityHeight = entityHeight;
    }

    public float getEntityRotation() {
        return entityRotation;
    }

    public void setEntityRotation(float entityRotation) {
        this.entityRotation = entityRotation;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    public AttributeHolder getAttributes() {
        return attributes;
    }

}
