/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.ui.StyledTextTable;
import com.dodecahelix.libgdx.template.view.resource.FontStyle;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;

/**
 *   Screen displaying a scrolling list of text lines (centered or left aligned)
 *   Button on bottom returns to main menu
 *
 * @author dpeters
 *
 */
public class CreditsScreen extends InformationScreen {

    CreditsText creditsText;

    public CreditsScreen(ControllerMap controllers,
                         Properties options,
                         CreditsText creditsText) {

        super(controllers, options);
        this.creditsText = creditsText;
    }

    @Override
    public Actor buildContentActor() {
        ResourceController resources = controllers.getResourceController();

        String defaultFontStyleName = properties.getStringProperty(PropertyName.UI_DEFAULT_FONT.name());
        FontStyle defaultFontStyle = FontStyle.getByName(defaultFontStyleName);

        StyledTextTable creditsContent = new StyledTextTable(resources, properties, defaultFontStyle);
        creditsContent.setMaximiumTextLines(Integer.MAX_VALUE);
        creditsContent.addWallOfText(creditsText);

        return creditsContent;
    }

    @Override
    public void executeDone() {
    }

    @Override
    public String getScreenTitle() {
        return "Credits";
    }

}
