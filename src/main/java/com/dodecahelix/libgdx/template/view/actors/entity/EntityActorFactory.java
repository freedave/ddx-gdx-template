/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors.entity;

import com.dodecahelix.libgdx.template.model.Entity;

public interface EntityActorFactory {

    /**
     *  Initialize the pools that generate actors for the stage
     *
     */
    public void init();

    /**
     *   Builds a stage actor that represents an entity in the data model, based on the entityType
     *
     * @param entityTypeId
     * @param entityId
     * @return
     */
    public EntityActor<? extends Entity> buildFromEntity(Entity entity);

}
