/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 *   Button with an icon to the left of a text label
 *
 */
public class IconTextButton extends Button {

    private final Image icon;
    private final Label label;

    private IconTextButtonStyle style;

    public IconTextButton(String text, Skin skin) {
        this(text, skin.get(IconTextButtonStyle.class));
        setSkin(skin);
    }

    public IconTextButton(String text, Skin skin, String styleName) {
        this(text, skin.get(styleName, IconTextButtonStyle.class));
        setSkin(skin);
    }

    public IconTextButton(String text, IconTextButtonStyle style) {
        this(text, 0, style);
    }

    public IconTextButton(String text, int iconSize, IconTextButtonStyle style) {
        super(style);
        this.style = style;

        defaults().space(3);

        icon = new Image();

        if (iconSize > 0) {
            float spacing = iconSize / 3.0f;
            add(icon).left().height(iconSize).width(iconSize).padRight(spacing);
        } else {
            add(icon).left();
        }

        label = new Label(text, new LabelStyle(style.font, style.fontColor));
        add(label);

        // TODO - hacky
        add(new Label("", new LabelStyle(style.font, style.fontColor))).expandX();

        setStyle(style);

        setSize(getPrefWidth(), getPrefHeight());
    }

    public void setStyle(ButtonStyle style) {
        if (!(style instanceof IconTextButtonStyle))
            throw new IllegalArgumentException("style must be a IconTextButtonStyle.");
        super.setStyle(style);
        this.style = (IconTextButtonStyle) style;
        if (icon != null) updateImage();
        if (label != null) {
            IconTextButtonStyle textButtonStyle = (IconTextButtonStyle) style;
            LabelStyle labelStyle = label.getStyle();
            labelStyle.font = textButtonStyle.font;
            labelStyle.fontColor = textButtonStyle.fontColor;
            label.setStyle(labelStyle);
        }
    }

    public IconTextButtonStyle getStyle() {
        return style;
    }

    private void updateImage() {

        if (style.icon != null) {
            icon.setDrawable(style.icon);
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        updateImage();
        Color fontColor;

        if (isDisabled() && style.disabledFontColor != null)
            fontColor = style.disabledFontColor;
        else if (isPressed() && style.downFontColor != null)
            fontColor = style.downFontColor;
        else if (isChecked() && style.checkedFontColor != null)
            fontColor = (isOver() && style.checkedOverFontColor != null) ? style.checkedOverFontColor : style.checkedFontColor;
        else if (isOver() && style.overFontColor != null)
            fontColor = style.overFontColor;
        else
            fontColor = style.fontColor;

        if (fontColor != null) label.getStyle().fontColor = fontColor;
        super.draw(batch, parentAlpha);
    }

    public Image getIcon() {
        return icon;
    }

    public Cell<?> getIconCell() {
        return getCell(icon);
    }

    public Label getLabel() {
        return label;
    }

    public Cell<?> getLabelCell() {
        return getCell(label);
    }

    public void setText(String text) {
        label.setText(text);
    }

    public CharSequence getText() {
        return label.getText();
    }

    static public class IconTextButtonStyle extends TextButtonStyle {

        public Drawable icon;

        public IconTextButtonStyle() {
        }

        public IconTextButtonStyle(Drawable up, Drawable down, Drawable checked, BitmapFont font) {
            super(up, down, checked, font);
        }

        public IconTextButtonStyle(IconTextButtonStyle style) {
            super(style);
            if (style.icon != null) this.icon = style.icon;
        }

        public IconTextButtonStyle(TextButtonStyle style) {
            super(style);
        }
    }

}
