/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors.debris;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.math.twodim.collision.CollisionCircle;
import com.dodecahelix.libgdx.template.math.twodim.collision.CollisionUtil;
import com.dodecahelix.libgdx.template.util.RandomNumberGenerator;

public class DebrisHandler {

    /**
     *  pool of reusable debris objects so that we don't need to keep creating new Images and Actions
     */
    private Pool<Debris> debrisPool;

    public DebrisHandler() {
    }

    public void initDebrisPool(final TextureRegion debrisTexture, int debrisPoolSize) {
        debrisPool = new Pool<Debris>(debrisPoolSize) {
            @Override
            protected Debris newObject() {
                return new Debris(this, debrisTexture);
            }
        };

        // initially populate the pool so that instantiation doesn't occur in game
        Array<Debris> initialSet = new Array<Debris>();
        for (int i = 0; i < debrisPoolSize; i++) {
            initialSet.add(debrisPool.obtain());
        }
        debrisPool.freeAll(initialSet);
    }

    public void displayCollisionDebris(Stage stage, CollisionCircle sourceCircle, CollisionCircle targetCircle) {

        if (debrisPool == null) {
            throw new IllegalStateException("Debris pool has not been initialized.");
        }

        float midpointX = CollisionUtil.getCollisionMidpointX(sourceCircle, targetCircle);
        float midpointY = CollisionUtil.getCollisionMidpointY(sourceCircle, targetCircle);

        for (int i = 0; i < SharedConstants.COLLISION_DEBRIS_COUNT; i++) {
            Debris debris = debrisPool.obtain();

            int destX = RandomNumberGenerator.getInstance().randomInt(Math.round(stage.getWidth()));
            int destY = RandomNumberGenerator.getInstance().randomInt(Math.round(stage.getHeight()));
            debris.reset(midpointX, midpointY, destX, destY);

            stage.addActor(debris);
        }

    }

}
