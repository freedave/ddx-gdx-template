/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.dodecahelix.libgdx.template.view.resource.TextureKey;


public class EntityBackgroundTilesActor extends EntityImageActor {

    public EntityBackgroundTilesActor(TextureKey bgTextureTile) {
        super(bgTextureTile);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        if (isInitialized()) {
            // is it a performance hit to redraw the tiles every frame, or a necessity?

            float screenWidth = this.getStage().getWidth();
            float screenHeight = this.getStage().getHeight();

            // stagger the tiles off the centerpoint of the screen
            float staggerX = (getScreenX() % imgWidth);
            float staggerY = (getScreenY() % imgHeight);

            Color originalColor = batch.getColor();
            batch.setColor(this.getColor());
            for (float x = -staggerX; x < screenWidth; x += imgWidth) {
                for (float y = -staggerY; y < screenHeight; y += imgHeight) {
                    batch.draw(textureTile, x, y, imgWidth, imgHeight);
                }
            }

            batch.setColor(originalColor);
        }

    }

}
