/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.screens;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.types.ScreenType.CommonScreen;
import com.dodecahelix.libgdx.template.view.View;

/**
 *   This is a simple screen.  Called once when the game is started, brings up/fades in splash screen
 *   while the game resources are loaded.
 *
 *   Being the pre-load screen, this will have minimal access to application resources, since these have not been initialized
 *
 * @author petersd
 *
 */
public class SplashScreen extends View {

    private Texture spinnerTexture;
    private Image spinnerImage;

    private Stage stage;

    Properties properties;

    public SplashScreen(ControllerMap controllers,
                        Properties options) {
        super(controllers, options);

        this.properties = options;
    }

    public void init() {
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport);
    }

    @Override
    public Set<SpaceId> getAssociatedSpaces() {
        return new HashSet<SpaceId>();
    }

    @Override
    public void show() {
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Showing splash screen.");

        // since this screen will only be called once, shouldn't we just need to put this in the constructor?
        // Answer : Can't count on that
        String spinnerTextureKey = properties.getStringProperty(PropertyName.SPLASH_SCREEN_TEXTURE.name());

        spinnerTexture = new Texture(Gdx.files.internal(spinnerTextureKey));
        spinnerImage = new Image(new TextureRegionDrawable(new TextureRegion(spinnerTexture)), Scaling.none);
        spinnerImage.setFillParent(true);

        // this is needed for the fade-in effect to work correctly; we're just making the image completely transparent
        spinnerImage.getColor().a = 0f;

        Action initGameAction = new Action() {
            @Override
            public boolean act(float delta) {
                // initialize resources here
                controllers.getResourceController().init(properties);
                controllers.getResourceController().loadMedia();

                // initialize model here?
                controllers.getModelController().resetModel();

                return true;
            }
        };

        // Set the splash image origin at the center of the screen, so it rotates about its center
        spinnerImage.setOrigin(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);

        spinnerImage.addAction(
        Actions.parallel(
        Actions.sequence(Actions.delay(3.0f), initGameAction),
        Actions.fadeIn(2.0f),
        Actions.forever(Actions.rotateBy(1.0f))
        //Actions.color(Color.BLUE),
        //Actions.scaleTo(0.5f, 0.5f)
        ));

        // and finally we add the actor to the stage
        stage.addActor(spinnerImage);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void render(float delta) {
        stage.act(delta);

        // clear the screen with the given RGB color (grey)
        Gdx.gl.glClearColor(0.05f, 0.05f, 0.05f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // This will make a call to keep loading the resource, until complete
        if (controllers.getResourceController().isMediaLoaded()) {
            Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Resources are loaded.  Loading view data.");

            // do any post-resource initialization.  This may freeze the splash screen.
            initViews();

            this.getControllerMap().getScreenController().setScreen(CommonScreen.MAIN_MENU.getId());
        } else {
            stage.draw();
        }
    }

    /**
     *   Runs any view initialization code that requires that the resources be loaded
     */
    private void initViews() {
        controllers.getScreenController().initScreens();

        // this is a problem - the resources may not be ready for the actor pools
        controllers.getStageController().init();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void hide() {
        // dispose the splash page, since we don't want it taking up resources
        spinnerTexture.dispose();

        // clear the stage, removing all actors (can be recreated if the screen is recalled)
        stage.clear();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

}
