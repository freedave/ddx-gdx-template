/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.screens;

import java.util.HashSet;
import java.util.Set;

import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.view.View;

/**
 *   Show a Goodbye text while resources are being unloaded
 *
 * @author dpeters
 *
 */
public class ExitScreen extends View {

    public ExitScreen(ControllerMap controllers,
                      Properties options) {

        super(controllers, options);
    }

    @Override
    public Set<SpaceId> getAssociatedSpaces() {
        // If the screen doesn't care about the model (or wants to pause it), return an empty HashSet
        return new HashSet<SpaceId>();
    }

    @Override
    public void render(float delta) {
        // TODO - Render a Goodby message and let it fade out before disposing
    }

    @Override
    public void show() {
        this.dispose();
    }

    @Override
    public void dispose() {
        this.getControllerMap().getResourceController().disposeAllMediaResources();
    }

}
