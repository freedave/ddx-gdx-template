/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.actors;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Pool;
import com.dodecahelix.libgdx.template.config.SharedConstants;

public class ScoreTable extends Table {

    private int[] score = new int[SharedConstants.MAX_SCORE_DIGITS];

    private Pool<Label> zeros;
    private Pool<Label> ones;
    private Pool<Label> twos;
    private Pool<Label> threes;
    private Pool<Label> fours;
    private Pool<Label> fives;
    private Pool<Label> sixes;
    private Pool<Label> sevens;
    private Pool<Label> eights;
    private Pool<Label> nines;

    public ScoreTable(Skin skin) {

        zeros = buildPool("0", skin);
        ones = buildPool("1", skin);
        twos = buildPool("2", skin);
        threes = buildPool("3", skin);
        fours = buildPool("4", skin);
        fives = buildPool("5", skin);
        sixes = buildPool("6", skin);
        sevens = buildPool("7", skin);
        eights = buildPool("8", skin);
        nines = buildPool("9", skin);

        for (int i = 0; i < SharedConstants.MAX_SCORE_DIGITS; i++) {
            this.add(zeros.obtain());
            this.add(zeros.obtain());
            this.add(zeros.obtain());
            this.add(zeros.obtain());
            this.add(zeros.obtain());
        }

        updateScore(0);
    }

    public void updateScore(int scoreInt) {
        scoreToIntArray(scoreInt);
        displayScore();
    }

    private void scoreToIntArray(int scoreInt) {
        int count = scoreInt;
        for (int i = SharedConstants.MAX_SCORE_DIGITS; i > 0; i--) {
            int digit = count % 10;
            if (count >= 10) {
                count = count / 10;
                score[i - 1] = digit;
            } else {
                if (count > 0) {
                    score[i - 1] = digit;
                    count = 0;
                } else {
                    score[i - 1] = 0;
                }
            }
        }
    }

    private void displayScore() {

        this.clear();

        for (int i = 0; i < SharedConstants.MAX_SCORE_DIGITS; i++) {
            int digit = score[i];
            switch (digit) {
                case 0:
                    this.add(zeros.obtain());
                    break;
                case 1:
                    this.add(ones.obtain());
                    break;
                case 2:
                    this.add(twos.obtain());
                    break;
                case 3:
                    this.add(threes.obtain());
                    break;
                case 4:
                    this.add(fours.obtain());
                    break;
                case 5:
                    this.add(fives.obtain());
                    break;
                case 6:
                    this.add(sixes.obtain());
                    break;
                case 7:
                    this.add(sevens.obtain());
                    break;
                case 8:
                    this.add(eights.obtain());
                    break;
                case 9:
                    this.add(nines.obtain());
                    break;
            }
        }
    }

    private Pool<Label> buildPool(final String digitString, final Skin skin) {
        Pool<Label> labelPool = new Pool<Label>(SharedConstants.MAX_SCORE_DIGITS) {
            @Override
            protected Label newObject() {
                Label label = new Label(digitString, skin);
                return label;
            }
        };

        // create three of each
        labelPool.obtain();
        labelPool.obtain();
        labelPool.obtain();

        return labelPool;
    }


}
