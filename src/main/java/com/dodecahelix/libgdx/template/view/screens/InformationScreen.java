/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.screens;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.types.ScreenType.CommonScreen;
import com.dodecahelix.libgdx.template.ui.SkinClass;
import com.dodecahelix.libgdx.template.view.View;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;

/**
 *  Abstract view class for simple screens that have a scrolling panel at the top with content, 
 *  and a "Done" button at the bottom
 *
 */
public abstract class InformationScreen extends View {

    public InformationScreen(ControllerMap controllers, Properties options) {
        super(controllers, options);
    }

    @Override
    public Set<SpaceId> getAssociatedSpaces() {
        // If the screen doesn't care about the model (or wants to pause it), return an empty HashSet
        return new HashSet<SpaceId>();
    }

    @Override
    public void show() {
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Showing Screen " + this.getClass());

        this.addTiledBackgroundTexture();
        this.buildContentFrame();

        // set the stage as the input processor
        Gdx.input.setInputProcessor(stage);
    }

    /**
     *  Builds the content for the scrolling pane
     *
     * @return
     */
    public abstract Actor buildContentActor();

    /**
     *   Fixed header content above the scrolling pane
     * @param skin
     * @return
     */
    protected Actor buildHeaderActor(Skin skin) {
        Label headerLabel = new Label(getScreenTitle(), skin.get(SkinClass.XL_LABEL.name(), LabelStyle.class));
        return headerLabel;
    }


    public abstract String getScreenTitle();

    /**
     *  Additional actions performed when the Done button is pressed (other than returning to the Main Menu screen
     */
    public abstract void executeDone();

    private void buildContentFrame() {

        ResourceController resources = controllers.getResourceController();
        Skin skin = resources.getSkin();

        float margins = properties.getScaledFloatProperty(PropertyName.UI_DEFAULT_SCREEN_MARGINS.name());
        float buttonHeight = properties.getScaledFloatProperty(PropertyName.UI_BIG_BUTTON_HEIGHT.name());

        // double the margins
        margins = margins * 2;

        // This is the background table that indicates the full screen
        Table screenTable = new Table(resources.getSkin());
        screenTable.setFillParent(true);

        Bounds screenBounds = controllers.getScreenController().getScreenBounds();
        ScrollPane scroller = new ScrollPane(buildContentActor(), skin);
        scroller.setFadeScrollBars(false);
        scroller.setVariableSizeKnobs(false);

        // erase the default scrollpane background
        ScrollPaneStyle style = new ScrollPaneStyle(scroller.getStyle());
        style.background = null;
        scroller.setStyle(style);

        screenTable.add(buildHeaderActor(skin)).pad(margins);
        screenTable.row();

        // align the scroller
        screenTable.add(scroller)
        .width(screenBounds.getWidth() - margins * 2)
        //.fillY();
        ;
        screenTable.row();

        TextButton doneButton = new TextButton("Done", skin.get(SkinClass.XL_BUTTON.name(), TextButtonStyle.class));
        doneButton.pad(margins);
        doneButton.addListener(new ActorGestureListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                executeDone();

                controllers.getScreenController().setScreen(CommonScreen.MAIN_MENU.getId());
            }
        });

        screenTable.add(doneButton).height(buttonHeight).pad(margins);

        stage.addActor(screenTable);
    }

    @Override
    public void render(float delta) {
        stage.act(delta);

        // clear the screen with the given RGB color (black)
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
    }
}
