/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.view.resource;

public class TextureKey {

    public static final TextureKey BALL = new TextureKey(1, "sphere-grey", 31);
    public static final TextureKey BUTTON = new TextureKey(2, "button32", 15);
    public static final TextureKey BUTTON_PRESSED = new TextureKey(3, "button32-pressed", 15);
    public static final TextureKey BUTTON_DISABLED = new TextureKey(4, "button32-disabled", 15);
    public static final TextureKey CIRCLE_BUTTON = new TextureKey(5, "circle-button", 15);
    public static final TextureKey CIRCLE_BUTTON_PRESSED = new TextureKey(6, "circle-button-pressed", 15);
    public static final TextureKey PARTICLE = new TextureKey(7, "particle", 2);
    public static final TextureKey GREY = new TextureKey(8, "basic-grey", 4);
    public static final TextureKey WHITE = new TextureKey(9, "basic-white", 4);
    public static final TextureKey BLACK = new TextureKey(10, "basic-black", 4);
    public static final TextureKey PROGRESS_BAR = new TextureKey(11, "pbar", 4);
    public static final TextureKey PROGRESS_BAR_FRAME = new TextureKey(12, "pbar-frame", 4);
    public static final TextureKey WINDOW_DEFAULT_BACKGROUND = new TextureKey(13, "frame-square", 8);
    public static final TextureKey SCROLLBAR_FRAME_BACKGROUND = new TextureKey(14, "basic-black", 4);
    public static final TextureKey SCROLLBAR_BACKGROUND = new TextureKey(15, "scrollbar-vertical", 10);
    public static final TextureKey SCROLLBAR_HANDLE = new TextureKey(16, "ui-scrollbar-handle-v", 0);
    public static final TextureKey DIALOG_FRAME = new TextureKey(17, "window-simple", 10);
    public static final TextureKey SELECT_BOX_BACKGROUND = new TextureKey(18, "ui-selectbox", 8);
    public static final TextureKey LIST_BACKGROUND = new TextureKey(20, "basic-white", 4);

    private String resourcePath;

    // corner width used in nine-patch texturing (optional)
    private int cornerWidth;

    private int lookupId;

    public TextureKey(int lookupId, String resourcePath, int cornerWidth) {
        this.lookupId = lookupId;
        this.resourcePath = resourcePath;
        this.cornerWidth = cornerWidth;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public int getCornerWidth() {
        return cornerWidth;
    }

    public int getLookupId() {
        return lookupId;
    }

}
