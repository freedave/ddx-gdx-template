/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.config;

import com.dodecahelix.libgdx.template.ui.RgbColor;
import com.dodecahelix.libgdx.template.view.resource.FontStyle;
import com.dodecahelix.libgdx.template.view.resource.TextureKey;

/**
 *   Standard properties used in the templates
 *
 *   Can be overridden : See ApplicationProperties in the sample app
 *
 *
 */
public enum PropertyName {

    ENTITY_CAPACITY(PropertyType.INT, 1000),
    SKIN_FILE_PATH(PropertyType.STRING, "skin/uiskin.json"),
    TEXTURE_FILE_PATH(PropertyType.STRING, "textures/texture.atlas"),
    ICON_FILE_PATH(PropertyType.STRING, "icons/icon.atlas"),
    LOG_ENTITY_CREATION(PropertyType.BOOLEAN, false),
    LOG_COLLISIONS(PropertyType.BOOLEAN, false),
    COLLISION_RESTITUTION(PropertyType.FLOAT, 1.0f),
    DEBRIS_POOL_SIZE(PropertyType.INT, 250),
    CONSOLE_MAX_LINES(PropertyType.INT, 50),
    SPLASH_SCREEN_TEXTURE(PropertyType.STRING, "textures/splash-world.png"),

    WELCOME_MESSAGE(PropertyType.STRING, "Welcome to the App!"),

    UI_BACKGROUND_TILE(PropertyType.TEXTURE, TextureKey.BLACK),

    // Use the same font throughout for consistency (even menu's)
    UI_DEFAULT_FONT(PropertyType.STRING, FontStyle.ARIMO.getDisplayName()),

    UI_XS_FONT_SIZE(PropertyType.SCALED_INT, 10),
    UI_SMALL_FONT_SIZE(PropertyType.SCALED_INT, 14),
    UI_MEDIUM_FONT_SIZE(PropertyType.SCALED_INT, 16),
    UI_LARGE_FONT_SIZE(PropertyType.SCALED_INT, 20),
    UI_XL_FONT_SIZE(PropertyType.SCALED_INT, 24),
    UI_XXL_FONT_SIZE(PropertyType.SCALED_INT, 36),

    // this is dynamic - changeable from settings
    UI_STANDARD_FONT_SIZE(PropertyType.SCALED_INT, 18),

    UI_LABEL_FONT_COLOR(PropertyType.STRING, RgbColor.BLACK.name()),

    UI_BUTTON_FONT_COLOR(PropertyType.STRING, RgbColor.WHITE.name()),
    UI_BUTTON_DISABLED_FONT_COLOR(PropertyType.STRING, RgbColor.GREY_SLATEGREY.name()),
    UI_BUTTON_BACKGROUND_COLOR(PropertyType.STRING, RgbColor.GREEN_SPRINGGREEN.name()),

    UI_STANDARD_BUTTON_HEIGHT(PropertyType.SCALED_FLOAT, 25f),
    UI_BIG_BUTTON_HEIGHT(PropertyType.SCALED_FLOAT, 40f),
    UI_DEFAULT_SCREEN_MARGINS(PropertyType.SCALED_FLOAT, 15.0f),
    UI_STANDARD_PADDING(PropertyType.SCALED_FLOAT, 10.0f),

    SCREEN_BUTTON_FONT_SIZE(PropertyType.SCALED_INT, 24),

    CONSOLE_COLOR_SYSTEM_FONT(PropertyType.STRING, RgbColor.WHITE.name()),
    CONSOLE_FONT_STYLE(PropertyType.STRING, FontStyle.ARIMO.getDisplayName()),
    CONSOLE_LINE_SPACING(PropertyType.SCALED_FLOAT, 10.0f),

    DEFAULT_BUTTON_UP_TEXTURE(PropertyType.TEXTURE, TextureKey.BUTTON),
    DEFAULT_BUTTON_DOWN_TEXTURE(PropertyType.TEXTURE, TextureKey.BUTTON_PRESSED),
    DEFAULT_BUTTON_DISABLED_TEXTURE(PropertyType.TEXTURE, TextureKey.BUTTON_DISABLED),

    DEFAULT_SCROLLBAR_FRAME_TEXTURE(PropertyType.TEXTURE, TextureKey.SCROLLBAR_FRAME_BACKGROUND),
    DEFAULT_SCROLLBAR_BG_TEXTURE(PropertyType.TEXTURE, TextureKey.SCROLLBAR_BACKGROUND),
    DEFAULT_SCROLLBAR_HANDLE_TEXTURE(PropertyType.TEXTURE, TextureKey.SCROLLBAR_HANDLE),
    DIALOG_FRAME_TEXTURE(PropertyType.TEXTURE, TextureKey.DIALOG_FRAME),

    DEFAULT_LIST_BG_TEXTURE(PropertyType.TEXTURE, TextureKey.LIST_BACKGROUND),
    DEFAULT_SELECTBOX_TEXTURE(PropertyType.TEXTURE, TextureKey.SELECT_BOX_BACKGROUND),

    DEBUG_MODE(PropertyType.BOOLEAN, false),;

    private PropertyType type;
    private Object defaultValue;

    PropertyName(PropertyType type, Object defaultValue) {
        this.type = type;
        this.defaultValue = defaultValue;
    }

    public PropertyType getType() {
        return type;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

}
