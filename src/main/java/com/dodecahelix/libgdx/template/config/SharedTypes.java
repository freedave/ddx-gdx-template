/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.config;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Values;
import com.dodecahelix.libgdx.template.types.Attribute;
import com.dodecahelix.libgdx.template.types.EntityType;
import com.dodecahelix.libgdx.template.view.resource.MusicKey;
import com.dodecahelix.libgdx.template.view.resource.SoundKey;

/**
 *   Reference class for SharedTypes.  Can be extended to add custom/app-specific types to the type maps.
 *
 *   Provided to be able to iterate through type enumerations from the template library
 *
 * @author dpeters
 *
 */
public class SharedTypes {

    private static SharedTypes instance;

    protected IntMap<Attribute> attributes;
    protected IntMap<EntityType> entityTypes;

    protected Array<SoundKey> sounds;
    protected Array<MusicKey> music;

    private SharedTypes() {
        attributes = new IntMap<Attribute>();
        entityTypes = new IntMap<EntityType>();
        sounds = new Array<SoundKey>(false, 10, SoundKey.class);
        music = new Array<MusicKey>(false, 10, MusicKey.class);

        loadCommonTypes();
    }

    public static SharedTypes getInstance() {
        if (instance == null) {
            instance = new SharedTypes();
        }
        return instance;
    }

    private void loadCommonTypes() {
        for (Attribute.StandardAttribute attribute : Attribute.StandardAttribute.values()) {
            attributes.put(attribute.ordinal(), new Attribute(attribute));
        }
        for (EntityType.CommonEntity entity : EntityType.CommonEntity.values()) {
            entityTypes.put(entity.ordinal(), new EntityType(entity));
        }

        // These are here as an example
        addSound(SoundKey.BEEP);
        addMusic(MusicKey.SONG);
    }

    public void addSound(SoundKey sound) {
        sounds.add(sound);
    }

    public Iterator<SoundKey> getSounds() {
        return sounds.iterator();
    }

    public void addMusic(MusicKey song) {
        music.add(song);
    }

    public Iterator<MusicKey> getMusic() {
        return music.iterator();
    }

    public void addEntityType(EntityType type) {
        entityTypes.put(type.getCode(), type);
    }

    public void addAttribute(Attribute att) {
        attributes.put(att.getCode(), att);
    }

    public Values<Attribute> getAttributes() {
        return attributes.values();
    }

    public Attribute attribute(int attributeId) {
        return attributes.get(attributeId);
    }

    public Values<EntityType> getEntityTypes() {
        return entityTypes.values();
    }

    public EntityType entityType(int entityTypeId) {
        return entityTypes.get(entityTypeId);
    }

}
