/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.config;

import java.util.Iterator;
import java.util.Map.Entry;

import com.badlogic.gdx.utils.IntFloatMap;
import com.badlogic.gdx.utils.IntIntMap;
import com.dodecahelix.libgdx.template.util.ScalingUtil;

public class PropertiesUtil {

    public static Properties overwriteWithUserPreferences(Properties properties, UserPreferences preferences) {
        for (Entry<String, Integer> preference : preferences.getIntegerPreferences().entrySet()) {
            properties.addIntegerProperty(preference.getKey(), preference.getValue());
        }
        for (Entry<String, Integer> preference : preferences.getScalableIntegerPreferences().entrySet()) {
            properties.addScaledIntegerProperty(preference.getKey(), preference.getValue());
        }
        for (Entry<String, String> preference : preferences.getStringPreferences().entrySet()) {
            properties.addStringProperty(preference.getKey(), preference.getValue());
        }
        for (Entry<String, Boolean> preference : preferences.getBooleanPreferences().entrySet()) {
            properties.addBooleanProperty(preference.getKey(), preference.getValue());
        }
        for (Entry<String, Float> preference : preferences.getFloatPreferences().entrySet()) {
            properties.addFloatProperty(preference.getKey(), preference.getValue());
        }
        for (Entry<String, Float> preference : preferences.getScalableFloatPreferences().entrySet()) {
            properties.addScaledFloatProperty(preference.getKey(), preference.getValue());
        }
        return properties;
    }

    /**
     * Applies screen size scaling to those properties identified as scalable
     * integers or scalable floats
     *
     * @param properties
     */
    public static void scaleProperties(Properties properties) {
        scaleIntegers(properties.getScaledIntProperties(), properties.getIntProperties());
        scaleFloats(properties.getScaledFloatProperties(), properties.getFloatProperties());
    }

    /**
     * Apply scaling to all scalable integers based on screen size
     *
     */
    private static void scaleIntegers(IntIntMap scaledIntProperties, IntIntMap intProperties) {
        IntIntMap scaledValues = new IntIntMap();

        Iterator<IntIntMap.Entry> intPropertyIterator = scaledIntProperties.iterator();
        IntIntMap.Entry entry;
        while (intPropertyIterator.hasNext()) {
            entry = intPropertyIterator.next();
            int unscaledValue = intProperties.get(entry.key, -1);
            if (unscaledValue < 0) {
                if (entry.value > 0) {
                    // do not scale!  This value should already be scaled
                    scaledValues.put(entry.key, entry.value);
                } else {
                    throw new IllegalStateException("There is no scaled or unscaled value for integer property with key " + entry.key);
                }
            } else {
                scaledValues.put(entry.key, ScalingUtil.scaleInteger(unscaledValue));
            }
            //intPropertyIterator.remove();
        }

        scaledIntProperties.clear();
        scaledIntProperties.putAll(scaledValues);
    }

    /**
     * Apply scaling to all scalable floats based on screen size
     *
     */
    private static void scaleFloats(IntFloatMap scaledFloatProperties, IntFloatMap floatProperties) {
        IntFloatMap scaledValues = new IntFloatMap();

        Iterator<IntFloatMap.Entry> floatPropertyIterator = scaledFloatProperties.iterator();
        IntFloatMap.Entry entry;
        while (floatPropertyIterator.hasNext()) {
            entry = floatPropertyIterator.next();
            float unscaledValue = floatProperties.get(entry.key, -1);
            if (unscaledValue < 0) {
                if (entry.value > 0) {
                    // do not scale!  This value should already be scaled
                    scaledValues.put(entry.key, entry.value);
                } else {
                    throw new IllegalStateException("There is no scaled or unscaled value for float property with key " + entry.key);
                }
            } else {
                scaledValues.put(entry.key, ScalingUtil.scaleFloat(unscaledValue));
            }
            //floatPropertyIterator.remove();
        }

        scaledFloatProperties.clear();
        scaledFloatProperties.putAll(scaledValues);
    }

}
