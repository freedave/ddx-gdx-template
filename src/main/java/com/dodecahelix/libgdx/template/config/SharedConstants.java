/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.config;

/**
 *   Constants for the template that shouldn't change - same for every application.
 *
 *   Configurable properties belong in SharedProperties
 *
 * @author dpeters
 *
 */
public class SharedConstants {

    public static final String LOGGING_TAG_LIFECYCLE = "lifecycle";
    public static final String LOGGING_TAG_ENTITY_CREATION = "entity.create";
    public static final String LOGGING_TAG_COLLISIONS = "collision";
    public static final String LOGGING_TAG_VIEW = "view";
    public static final String LOGGING_TAG_GAME = "game";
    public static final String LOGGING_TAG_APPLICATION = "application";

    // Use this width as a reference point for scaling graphics (i.e 1200x768 = scaling of 1200/800 = 1.5)
    public static final float SCALING_REFERENCE_WIDTH = 800f;

    // Resource location (relative to Android /assets folder)
    public static final String TEXTURE_ATLAS_LOCATION = "textures/texture.atlas";
    public static final String ICON_ATLAS_LOCATION = "icons/icon.atlas";
    public static final String SKIN_FILE_PATH = "skin/uiskin.json";

    // maximum number of digits to be displayed in the score
    public static final int MAX_SCORE_DIGITS = 6;

    // number of debris particles for a single collision
    public static final int COLLISION_DEBRIS_COUNT = 10;
    public static final int EVENT_POOL_LIMIT = 10;

}
