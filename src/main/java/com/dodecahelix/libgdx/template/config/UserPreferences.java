/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.config;

import java.util.HashMap;
import java.util.Map;

public class UserPreferences {

    private Map<String, Integer> integerPreferences = new HashMap<String, Integer>();
    private Map<String, String> stringPreferences = new HashMap<String, String>();
    private Map<String, Float> floatPreferences = new HashMap<String, Float>();
    private Map<String, Boolean> booleanPreferences = new HashMap<String, Boolean>();
    private Map<String, Integer> scalableIntegerPreferences = new HashMap<String, Integer>();
    private Map<String, Float> scalableFloatPreferences = new HashMap<String, Float>();

    public UserPreferences() {
    }

    public Map<String, Integer> getIntegerPreferences() {
        return integerPreferences;
    }

    public void setIntegerPreferences(Map<String, Integer> integerPreferences) {
        this.integerPreferences = integerPreferences;
    }

    public Map<String, String> getStringPreferences() {
        return stringPreferences;
    }

    public void setStringPreferences(Map<String, String> stringPreferences) {
        this.stringPreferences = stringPreferences;
    }

    public Map<String, Float> getFloatPreferences() {
        return floatPreferences;
    }

    public void setFloatPreferences(Map<String, Float> floatPreferences) {
        this.floatPreferences = floatPreferences;
    }

    public Map<String, Boolean> getBooleanPreferences() {
        return booleanPreferences;
    }

    public void setBooleanPreferences(Map<String, Boolean> booleanPreferences) {
        this.booleanPreferences = booleanPreferences;
    }

    public Map<String, Integer> getScalableIntegerPreferences() {
        return scalableIntegerPreferences;
    }

    public void setScalableIntegerPreferences(Map<String, Integer> scalableIntegerPreferences) {
        this.scalableIntegerPreferences = scalableIntegerPreferences;
    }

    public Map<String, Float> getScalableFloatPreferences() {
        return scalableFloatPreferences;
    }

    public void setScalableFloatPreferences(Map<String, Float> scalableFloatPreferences) {
        this.scalableFloatPreferences = scalableFloatPreferences;
    }

}
