/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.config;

import com.badlogic.gdx.utils.IntFloatMap;
import com.badlogic.gdx.utils.IntIntMap;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.dodecahelix.libgdx.template.view.resource.TextureKey;

/**
 * Container for application properties.
 *
 * <p>
 * Stores properties in libgdx primitive Map's
 */
public class Properties {

    // maps property names to an identifier for quicker mapping
    private ObjectIntMap<String> propertyKeys;

    private IntFloatMap floatProperties;
    private IntIntMap intProperties;
    private IntMap<String> stringProperties;
    private IntIntMap booleanProperties;
    private IntMap<TextureKey> textureProperties;

    private IntIntMap scaledIntProperties;
    private IntFloatMap scaledFloatProperties;

    public Properties() {
        propertyKeys = new ObjectIntMap<String>();
        floatProperties = new IntFloatMap();
        intProperties = new IntIntMap();
        stringProperties = new IntMap<String>();
        booleanProperties = new IntIntMap();
        textureProperties = new IntMap<TextureKey>();

        scaledIntProperties = new IntIntMap();
        scaledFloatProperties = new IntFloatMap();

        loadDefaultPropertyValues();
    }

    private void loadDefaultPropertyValues() {

        for (PropertyName propertyName : PropertyName.values()) {
            switch (propertyName.getType()) {
                case STRING:
                    addStringProperty(propertyName.name(), propertyName.getDefaultValue().toString());
                    break;
                case INT:
                    addIntegerProperty(propertyName.name(), Integer.parseInt(propertyName.getDefaultValue().toString()));
                    break;
                case FLOAT:
                    addFloatProperty(propertyName.name(), Float.parseFloat(propertyName.getDefaultValue().toString()));
                    break;
                case BOOLEAN:
                    addBooleanProperty(propertyName.name(), Boolean.parseBoolean(propertyName.getDefaultValue().toString()));
                    break;
                case TEXTURE:
                    addTextureProperty(propertyName.name(), propertyName.getDefaultValue());
                    break;
                case SCALED_FLOAT:
                    addScalableFloatProperty(propertyName.name(), Float.parseFloat(propertyName.getDefaultValue().toString()));
                    break;
                case SCALED_INT:
                    addScalableIntegerProperty(propertyName.name(), Integer.parseInt(propertyName.getDefaultValue().toString()));
                    break;
            }
        }
    }

    public void addTextureProperty(String propertyName, Object defaultValue) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }
        textureProperties.put(propIdx, (TextureKey) defaultValue);
    }

    public void addBooleanProperty(String propertyName, boolean value) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }
        int intVal = 0;
        if (value) {
            intVal = 1;
        }
        booleanProperties.put(propIdx, intVal);
    }

    public void addStringProperty(String propertyName, String value) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }
        stringProperties.put(propIdx, value);
    }

    public void addIntegerProperty(String propertyName, int value) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }
        intProperties.put(propIdx, value);
    }

    /**
     * Set the unscaled value, to be scaled at initialization (or a resize event)
     *
     * @param propertyName
     * @param value
     */
    public void addScalableIntegerProperty(String propertyName, int value) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }
        // retain the unscaled value
        intProperties.put(propIdx, value);

        // start as 0, indicating not yet scaled
        scaledIntProperties.put(propIdx, -1);
    }

    /**
     * Set the scaled value.
     *
     * @param propertyName
     * @param value
     */
    public void addScaledIntegerProperty(String propertyName, int value) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }

        // set the unscaled value to -1, so the value is not rescaled (in PropertiesUtil.scaleIntegers)
        intProperties.put(propIdx, -1);

        // start as 0, indicating not yet scaled
        scaledIntProperties.put(propIdx, value);
    }

    public void addFloatProperty(String propertyName, float value) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }
        floatProperties.put(propIdx, value);
    }

    /**
     * Sets the unscaled value, to be scaled at initialization (or a resize event)
     *
     * @param propertyName
     * @param value
     */
    public void addScalableFloatProperty(String propertyName, float value) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }
        // retain the unscaled value
        floatProperties.put(propIdx, value);

        // start as 0, indicating not yet scaled
        scaledFloatProperties.put(propIdx, -1);
    }

    /**
     * Sets the scaled float property.  This will not be rescaled.
     *
     * @param propertyName
     * @param value
     */
    public void addScaledFloatProperty(String propertyName, float value) {
        int propIdx = propertyKeys.get(propertyName, -1);
        if (propIdx < 0) {
            propIdx = propertyKeys.size;
            propertyKeys.put(propertyName, propIdx);
        }

        // set the unscaled value to -1, so that this property will not be rescaled (in PropertiesUtil.scaleFloats)
        floatProperties.put(propIdx, -1);

        scaledFloatProperties.put(propIdx, value);
    }

    public float getFloatProperty(String propertyName, float defaultValue) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return floatProperties.get(propIdx, defaultValue);
    }

    public float getFloatProperty(String propertyName) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return floatProperties.get(propIdx, 0.0f);
    }

    public float getScaledFloatProperty(String propertyName) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return scaledFloatProperties.get(propIdx, 0.0f);
    }

    public int getIntProperty(String propertyName, int defaultValue) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return intProperties.get(propIdx, defaultValue);
    }

    public int getIntProperty(String propertyName) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return intProperties.get(propIdx, 0);
    }

    public int getScaledIntProperty(String propertyName) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return scaledIntProperties.get(propIdx, 0);
    }

    public String getStringProperty(String propertyName) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return stringProperties.get(propIdx);
    }

    public boolean getBooleanProperty(String propertyName) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return (booleanProperties.get(propIdx, 0) == 1);
    }

    public TextureKey getTextureProperty(String propertyName) {
        int propIdx = propertyKeys.get(propertyName, -1);
        return textureProperties.get(propIdx);
    }

    protected IntFloatMap getFloatProperties() {
        return floatProperties;
    }

    protected IntIntMap getIntProperties() {
        return intProperties;
    }

    protected IntIntMap getScaledIntProperties() {
        return scaledIntProperties;
    }

    protected IntFloatMap getScaledFloatProperties() {
        return scaledFloatProperties;
    }

    @Override
    public String toString() {

        StringBuffer toStringBuffer = new StringBuffer("Properties [ floatProperties:");

        IntFloatMap.Entries floats = floatProperties.entries();
        while (floats.hasNext()) {
            IntFloatMap.Entry floatProperty = floats.next();
            appendKeyValue(toStringBuffer, floatProperty.key, floatProperty.value);
        }

        toStringBuffer.append(" ... scalableFloatProperties:");

        floats = scaledFloatProperties.entries();
        while (floats.hasNext()) {
            IntFloatMap.Entry floatProperty = floats.next();
            appendKeyValue(toStringBuffer, floatProperty.key, floatProperty.value);
        }

        toStringBuffer.append(" ... intProperties:");

        IntIntMap.Entries ints = intProperties.entries();
        while (ints.hasNext()) {
            IntIntMap.Entry intProperty = ints.next();
            appendKeyValue(toStringBuffer, intProperty.key, intProperty.value);
        }

        toStringBuffer.append(" ... scalableIntProperties:");

        ints = scaledIntProperties.entries();
        while (ints.hasNext()) {
            IntIntMap.Entry intProperty = ints.next();
            appendKeyValue(toStringBuffer, intProperty.key, intProperty.value);
        }

        toStringBuffer.append(" ... stringProperties:");
        IntMap.Entries<String> strings = stringProperties.entries();
        while (strings.hasNext()) {
            IntMap.Entry<String> stringProperty = strings.next();
            appendKeyValue(toStringBuffer, stringProperty.key, stringProperty.value);
        }

        toStringBuffer.append(" ]");
        return toStringBuffer.toString();
    }

    public void appendKeyValue(StringBuffer toStringBuffer, int key, Object value) {
        toStringBuffer.append(" ");
        toStringBuffer.append(propertyKeys.findKey(key));
        toStringBuffer.append("=");
        toStringBuffer.append(value);
        toStringBuffer.append(";");
    }

}
