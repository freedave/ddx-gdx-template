/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.util;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.libgdx.template.config.SharedConstants;

public class ScalingUtil {

    /**
     * Return the factor to which the screen size compares to a reference size
     * (800) using the larger of either width or height.
     *
     * <p> i.e; if the screen size is 2048x1536, the factor would be 2048/800 = 2.56
     *
     * @return
     */
    public static float getScalingFactor() {

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        float scalingRef = width;
        if (height > width) {
            scalingRef = height;
        }

        return (scalingRef / SharedConstants.SCALING_REFERENCE_WIDTH);
    }

    /**
     * Scale a number according to screen size - comparing it with a reference size (800px)
     *
     * <p>(i.e; if the screen width is 1600px, the number 10 will be doubled)
     *
     * @param number
     * @return
     */
    public static float scaleFloat(float number) {
        return number * getScalingFactor();
    }

    /**
     * Scales an integer and rounds it to the nearest even number;
     *
     * @param unscaledValue
     *
     * @return
     */
    public static int scaleInteger(int unscaledValue) {

        float scaled = scaleFloat(unscaledValue);

        float unroundedHalf = scaled / 2;
        int rounded = (int) (unroundedHalf + 0.5f);
        if (unroundedHalf < 0) {
            rounded = (int) (unroundedHalf - 0.5f);
        }
        return rounded * 2;
    }

}
