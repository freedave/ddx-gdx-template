/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.control;

import java.util.Set;

import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.access.impl.SpaceStateAccessor;

/**
 *   When a view's show() method is called, this controller will activate or deactivate the associated spaces for the view
 *
 * @author dpeters
 *
 */
public class SpaceController {

    SpaceStateAccessor spaceStateAccessor;

    public SpaceController(SpaceStateAccessor spaceStateAccessor) {
        super();
        this.spaceStateAccessor = spaceStateAccessor;
    }

    public void activateSpaces(Set<SpaceId> associatedSpaces) {
        spaceStateAccessor.spaceActivation(associatedSpaces);
    }

}
