/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.control;

import java.util.HashMap;
import java.util.Map;

import com.dodecahelix.libgdx.template.control.events.ViewEventController;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;

/**
 *   Container for all controllers that may be passed around easily to clients
 *
 *   Maintained by the master controller
 *
 * @author dpeters
 *
 */
public class ControllerMap {

    ResourceController resourceController;

    ModelController modelController;

    ScreenController screenController;

    BumpController bumpController;

    StageController stageController;

    EntitySynchController entityActorSyncController;

    SpaceController spaceController;

    ViewEventController actionController;

    PlatformController platformController;

    /*
     *   Additional controllers defined by the application
     */
    private Map<String, Controller> applicationControllers;

    public ControllerMap(ResourceController resourceController,
                         ModelController modelController,
                         ScreenController screenController,
                         BumpController bumpController,
                         StageController stageController,
                         EntitySynchController entityActorSyncController,
                         SpaceController spaceController,
                         ViewEventController actionController,
                         PlatformController platformController) {

        applicationControllers = new HashMap<String, Controller>();

        this.resourceController = resourceController;
        this.modelController = modelController;
        this.screenController = screenController;
        this.bumpController = bumpController;
        this.stageController = stageController;
        this.entityActorSyncController = entityActorSyncController;
        this.spaceController = spaceController;
        this.actionController = actionController;
        this.platformController = platformController;
    }

    public ResourceController getResourceController() {
        return resourceController;
    }

    public ModelController getModelController() {
        return modelController;
    }

    public ScreenController getScreenController() {
        return screenController;
    }

    public BumpController getBumpController() {
        return bumpController;
    }

    public StageController getStageController() {
        return stageController;
    }

    public EntitySynchController getEntityActorSyncController() {
        return entityActorSyncController;
    }

    public SpaceController getSpaceController() {
        return spaceController;
    }

    public void addApplicationController(String controllerKey, Controller controller) {
        applicationControllers.put(controllerKey, controller);
    }

    public Controller getApplicationController(String controllerKey) {
        return applicationControllers.get(controllerKey);
    }

    public ViewEventController getActionController() {
        return actionController;
    }

    public PlatformController getPlatformController() {
        return platformController;
    }

}
