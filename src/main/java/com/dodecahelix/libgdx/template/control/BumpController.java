/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.control;

import com.dodecahelix.libgdx.template.model.SpaceType;
import com.dodecahelix.libgdx.template.model.access.bump.BumpAccessor;
import com.dodecahelix.libgdx.template.model.access.bump.BumpAccessorMap;

/**
 *   Handles MODEL changes for each render (bump) step
 *
 * @author dpeters
 *
 */
public class BumpController {

    BumpAccessorMap bumpAccessors;

    public BumpController(BumpAccessorMap bumpAccessors) {
        this.bumpAccessors = bumpAccessors;
    }

    public void bump(float delta) {

        // each space type has its own generic bump method
        for (SpaceType spaceType : SpaceType.VALUES) {
            BumpAccessor bumper = bumpAccessors.getBumpAccessorForType(spaceType);
            if (bumper != null) {
                bumper.bumpSpace(delta, spaceType);
            }
        }

    }

}
