/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.control;

import com.badlogic.gdx.utils.LongMap;
import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.access.impl.EntityQueryAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.GridBoundsAccessor;
import com.dodecahelix.libgdx.template.model.attributes.AttributeHolder;
import com.dodecahelix.libgdx.template.types.Attribute.StandardAttribute;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityActor;

/**
 *   Sync's the actor attributes with the entity attributes, prior to render
 *
 * @author dpeters
 *
 */
public abstract class EntitySynchController {

    EntityQueryAccessor queryAccessor;

    GridBoundsAccessor gridBoundsAccessor;

    public EntitySynchController(EntityQueryAccessor queryAccessor,
                                 GridBoundsAccessor gridBoundsAccessor) {
        super();
        this.queryAccessor = queryAccessor;
        this.gridBoundsAccessor = gridBoundsAccessor;
    }

    public void updateActors(LongMap<EntityActor<? extends Entity>> actorMap, Bounds screenBounds) {

        for (EntityActor<? extends Entity> entityActor : actorMap.values()) {

            Entity cloneEntity = entityActor.getEntityClone();

            // TODO - this may not be efficient -- look for entities by Space instead of by Universe
            Entity modelEntity = queryAccessor.findEntityInActiveSpaces(cloneEntity.getId());
            if (modelEntity == null) {
                throw new IllegalStateException("Clone entity  " + cloneEntity + " has no assicated model entity.");
            }

            cloneEntity.synchronizeStaticFields(modelEntity);

            // these are default synch properties
            switch (modelEntity.getSpaceId().getType()) {
                case GRID:
                    updateGridActor(entityActor, modelEntity);
                    break;
                case USER_INTERFACE:
                    updateEntityActor(entityActor, modelEntity);
                    break;
                default:
                    updateEntityActor(entityActor, modelEntity);
                    break;
            }

            synchronizeEntities(entityActor, modelEntity);
        }
    }

    /**
     *   Additional application-specific synchronization, that may require other controllers/accessors
     *
     * @param actor
     * @param entity
     */
    public abstract void synchronizeEntities(EntityActor<? extends Entity> actor, Entity entity);

    /**
     *   Transfers all attributes from the entity to the entity actor
     *     (if it is desired to override this functionality, create a separate case statement (above) for the EntityActor)
     *
     * @param actor
     * @param entity
     */
    private void updateEntityActor(EntityActor<? extends Entity> actor, Entity entity) {

        AttributeHolder entityAttributes = entity.getAttributes();
        AttributeHolder actorAttributes = actor.getAttributes();

        actorAttributes.copyFrom(entityAttributes);
    }

    @SuppressWarnings("unused")
    private void updateInterface(EntityActor<? extends Entity> actor, Entity entity) {
        float screenX = entity.getAttributes().getFloatValue(StandardAttribute.POSITION_X.getId(), 0);
        float screenY = entity.getAttributes().getFloatValue(StandardAttribute.POSITION_Y.getId(), 0);
        float width = entity.getAttributes().getFloatValue(StandardAttribute.WIDTH.getId(), 0);
        float height = entity.getAttributes().getFloatValue(StandardAttribute.HEIGHT.getId(), 0);

        actor.setScreenX(screenX);
        actor.setScreenY(screenY);
        actor.setEntityWidth(width);
        actor.setEntityHeight(height);
    }

    private void updateGridActor(EntityActor<? extends Entity> actor, Entity entity) {

        float positionX = entity.getAttributes().getFloatValue(StandardAttribute.POSITION_X.getId(), 0);
        float positionY = entity.getAttributes().getFloatValue(StandardAttribute.POSITION_Y.getId(), 0);
        float rotation = entity.getAttributes().getFloatValue(StandardAttribute.FACING_ANGLE.getId(), 0);
        float size = entity.getAttributes().getFloatValue(StandardAttribute.SIZE.getId(), 0);

        // convert grid position to screen position
        SpaceId spaceId = entity.getSpaceId();
        Bounds viewportBounds = gridBoundsAccessor.getGridViewportBounds(spaceId);

        float screenX = positionX - viewportBounds.getMinX();
        float screenY = positionY - viewportBounds.getMinY();

        // TODO - scale actor size if viewportBounds != screenSize
        actor.setScreenX(screenX);
        actor.setScreenY(screenY);
        actor.setEntityRotation(rotation);
        actor.setEntityWidth(size);
        actor.setEntityHeight(size);
    }
}
