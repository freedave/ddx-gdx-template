/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.control;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.LongArray;
import com.badlogic.gdx.utils.LongMap;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.access.impl.EntityDeletionAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityLifecycleAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityQueryAccessor;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityActor;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityActorFactory;

/**
 *   Adds and removes entities from the stage according to the model state
 *
 * @author dpeters
 *
 */
public class StageController {

    EntityQueryAccessor entityQueryAccessor;

    EntityDeletionAccessor entityDeletionAccessor;

    EntityLifecycleAccessor entityLifecycleAccessor;

    EntityActorFactory actorFactory;

    Properties properties;

    /**
     *   Maps actors to their entity ID's
     */
    private LongMap<EntityActor<? extends Entity>> actorMap;


    public StageController(EntityQueryAccessor entityQueryAccessor,
                           EntityDeletionAccessor entityDeletionAccessor,
                           EntityLifecycleAccessor entityLifecycleAccessor,
                           EntityActorFactory actorFactory,
                           Properties properties) {

        super();
        this.entityQueryAccessor = entityQueryAccessor;
        this.entityDeletionAccessor = entityDeletionAccessor;
        this.entityLifecycleAccessor = entityLifecycleAccessor;
        this.actorFactory = actorFactory;
        this.properties = properties;
    }

    /**
     *   Initializes the controller during splash screen phase
     */
    public void init() {
        actorFactory.init();
    }

    /**
     *   Builds a stage from scratch
     * @return
     */
    public Stage build(Stage stage) {
        stage.clear();

        if (actorMap == null) {
            int entityCapacity = properties.getIntProperty(PropertyName.ENTITY_CAPACITY.name());
            actorMap = new LongMap<EntityActor<? extends Entity>>(entityCapacity);
        } else {
            actorMap.clear();
        }
        return stage;
    }

    /**
     *  Adds and removes actors from the UI depending on the entity state
     *
     * @param stage
     */
    public void updateStage(Stage stage) {

        // add actors for entities that have been initialized
        //  change their state to active
        LongArray initializedEntities = entityLifecycleAccessor.getInitializedEntities();
        for (int i = 0; i < initializedEntities.size; i++) {
            long entityId = initializedEntities.get(i);
            EntityActor<? extends Entity> actor = buildActor(entityId);

            stage.addActor(actor);
            actorMap.put(entityId, actor);
        }

        // initialized entities are now synchronized with the stage.  Clear the list of initialized entities
        entityLifecycleAccessor.activateInitializedEntities();

        // remove actors for entities that have been destroyed
        //  remove entities from the model
        LongArray destroyedEntities = entityLifecycleAccessor.getDestroyedEntities();
        for (int i = 0; i < destroyedEntities.size; i++) {
            long entityId = destroyedEntities.get(i);
            EntityActor<? extends Entity> actor = actorMap.remove(entityId);
            actor.remove();
            entityDeletionAccessor.addEntityToDeletionQueue(entityId);
        }

        entityDeletionAccessor.deleteQueuedEntities();
    }

    private EntityActor<? extends Entity> buildActor(long id) {
        Entity entity = entityQueryAccessor.findEntityInActiveSpaces(id);
        return actorFactory.buildFromEntity(entity);
    }

    public void setActorFactory(EntityActorFactory actorFactory) {
        this.actorFactory = actorFactory;
    }

    public LongMap<EntityActor<? extends Entity>> getActorMap() {
        return actorMap;
    }

    public void resize(int width, int height) {
        if (actorMap != null) {
            for (EntityActor<? extends Entity> actor : actorMap.values()) {
                actor.resize(width, height);
            }
        }
    }

}
