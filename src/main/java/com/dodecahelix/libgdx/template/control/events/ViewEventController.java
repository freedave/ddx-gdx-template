/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.control.events;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.model.access.impl.SpaceEventAccessor;

/**
 *   Queues actions, created from the view or actors, which are then passed to the model during the draw method
 *
 * @author dpeters
 *
 */
public class ViewEventController {

    private Pool<ViewEvent> eventPool;
    private Array<ViewEvent> activeEvents;

    SpaceEventAccessor spaceEventAccessor;

    public ViewEventController(SpaceEventAccessor spaceEventAccessor) {
        this.spaceEventAccessor = spaceEventAccessor;

        eventPool = new Pool<ViewEvent>(SharedConstants.EVENT_POOL_LIMIT) {
            @Override
            protected ViewEvent newObject() {
                return new ViewEvent();
            }
        };

        activeEvents = new Array<ViewEvent>(SharedConstants.EVENT_POOL_LIMIT);
    }

    public void handleActions() {
        int queueSize = activeEvents.size;

        for (int i = queueSize - 1; i >= 0; i--) {
            // remove 0 or i?
            ViewEvent event = activeEvents.removeIndex(i);
            if (event != null) {
                // send the event to all active spaces
                spaceEventAccessor.act(event);
                eventPool.free(event);
            }
        }
    }

    public ViewEvent generateEvent(int eventTypeId) {
        ViewEvent event = eventPool.obtain();
        event.setEventTypeId(eventTypeId);
        activeEvents.add(event);
        return event;
    }

}
