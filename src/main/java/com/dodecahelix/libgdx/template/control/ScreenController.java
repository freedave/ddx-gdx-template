/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.control;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.model.access.impl.EntityLifecycleAccessor;
import com.dodecahelix.libgdx.template.view.ScreenEvent;
import com.dodecahelix.libgdx.template.view.View;
import com.dodecahelix.libgdx.template.view.ViewMap;

/**
 *   Controls setting of the current screen
 *
 * @author dpeters
 *
 */
public class ScreenController {

    EntityLifecycleAccessor entityLifecycleAccessor;

    private Game game;
    private Bounds screenSize;

    private ViewMap views;

    public ScreenController(EntityLifecycleAccessor entityLifecycleAccessor) {
        this.entityLifecycleAccessor = entityLifecycleAccessor;

        screenSize = new Bounds(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    public void init(Game game, ViewMap views) {
        this.game = game;
        this.views = views;

        views.initMap();
    }

    /**
     *   Called after the splash screen is brought up
     */
    public void initScreens() {
        views.initViews();
    }

    public Bounds getScreenBounds() {
        return screenSize;
    }

    public View getScreen(int screenId) {
        return views.getView(screenId);
    }

    public void setScreen(int screenId) {
        View screen = getScreen(screenId);

        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "Setting screen to " + screen);

        game.setScreen(screen);
    }

    public void resize() {
        screenSize = new Bounds(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "Resizing screen to " + screenSize);

        // resize has occurred.  update resources
    }

    public void handleScreenEvent(ScreenEvent event) {
    }

    public void addView(int code, View applicationView) {
        views.add(code, applicationView);
    }


}
