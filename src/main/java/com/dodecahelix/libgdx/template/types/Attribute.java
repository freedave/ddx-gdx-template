/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.types;

import com.dodecahelix.libgdx.template.model.attributes.AttributeType;

public class Attribute extends ExtensibleEnumeratedType {

    public enum StandardAttribute {

        POSITION_X(AttributeType.FLOAT),
        POSITION_Y(AttributeType.FLOAT),
        SPEED(AttributeType.FLOAT),
        FACING_ANGLE(AttributeType.FLOAT),
        MOVEMENT_ANGLE(AttributeType.FLOAT),
        SIZE(AttributeType.FLOAT),
        DISPLAY_NAME(AttributeType.STRING),
        IS_ACTIVE(AttributeType.BOOLEAN),
        MASS(AttributeType.FLOAT),
        WIDTH(AttributeType.FLOAT),
        HEIGHT(AttributeType.FLOAT),
        FILL_X(AttributeType.BOOLEAN),
        FILL_Y(AttributeType.BOOLEAN),
        VISIBLE(AttributeType.BOOLEAN),
        MESSAGE(AttributeType.STRING),
        MESSAGE_COUNTER(AttributeType.INT);

        private AttributeType type;

        StandardAttribute(AttributeType type) {
            this.type = type;
        }

        public int getId() {
            return ordinal();
        }

        public AttributeType getType() {
            return type;
        }
    }

    ;

    private AttributeType type;

    public Attribute(String name, int code, AttributeType type) {
        super(name, code);
        this.type = type;
    }

    public Attribute(StandardAttribute attribute) {
        super(attribute.name(), attribute.getId());
        this.type = attribute.getType();
    }

    public AttributeType getType() {
        return type;
    }


}
