/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.ui;

import java.util.ArrayDeque;
import java.util.Queue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.text.TextHolder;
import com.dodecahelix.libgdx.template.text.TextLine;
import com.dodecahelix.libgdx.template.text.TextStyleAlign;
import com.dodecahelix.libgdx.template.text.TextStyleDecor;
import com.dodecahelix.libgdx.template.text.TextStyleSize;
import com.dodecahelix.libgdx.template.view.resource.FontStyle;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;

/**
 * A table with content as lines of text
 *
 */
public class StyledTextTable extends Table {

    private ResourceController resources;
    private Properties properties;
    private FontStyle font;

    private Queue<Label> textLines;
    private int lineIndex = 0;

    /**
     * Maximum number of text lines to contain in the fully scrollable view 
     *
     * TODO : should make this configurable
     *
     */
    private int maximumTextLines = 100;

    private LabelStyle tableLabelStyle;
    private LabelStyle tableLabelLargeStyle;
    private LabelStyle tableLabelMediumStyle;
    private LabelStyle tableLabelSmallStyle;

    private float lineSpacing;

    public StyledTextTable(ResourceController resourceController, Properties properties, FontStyle font) {
        super(resourceController.getSkin());

        this.resources = resourceController;
        this.properties = properties;
        this.font = font;

        initFonts();

        maximumTextLines = properties.getIntProperty(PropertyName.CONSOLE_MAX_LINES.name());
        textLines = new ArrayDeque<Label>(maximumTextLines);
    }

    public void setMaximiumTextLines(int maximumConsoleLines) {
        maximumTextLines = maximumConsoleLines;
    }

    public void updateFont(FontStyle fontStyle) {
        this.font = fontStyle;
        initFonts();
    }

    public void initFonts() {
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "(re)setting styled text fonts");
        Skin skin = resources.getSkin();

        // int standardFontSize =
        // resources.scalePropertyAsEvenInt(properties.getIntProperty(PropertyName.UI_STANDARD_FONT_SIZE.name()));
        int standardFontSize = properties.getScaledIntProperty(PropertyName.UI_STANDARD_FONT_SIZE.name());
        int largeFontSize = properties.getScaledIntProperty(PropertyName.UI_LARGE_FONT_SIZE.name());
        int smallFontSize = properties.getScaledIntProperty(PropertyName.UI_SMALL_FONT_SIZE.name());
        int mediumFontSize = properties.getScaledIntProperty(PropertyName.UI_MEDIUM_FONT_SIZE.name());

        BitmapFont standardFont = resources.findFont(font, standardFontSize);
        BitmapFont largeFont = resources.findFont(font, largeFontSize);
        BitmapFont smallFont = resources.findFont(font, smallFontSize);
        BitmapFont mediumFont = resources.findFont(font, mediumFontSize);

        // remove the default colors, this will interfere
        tableLabelStyle = new LabelStyle(skin.get(LabelStyle.class));
        tableLabelStyle.font = standardFont;
        tableLabelStyle.fontColor = null;
        tableLabelLargeStyle = new LabelStyle(skin.get(LabelStyle.class));
        tableLabelLargeStyle.font = largeFont;
        tableLabelLargeStyle.fontColor = null;
        tableLabelMediumStyle = new LabelStyle(skin.get(LabelStyle.class));
        tableLabelMediumStyle.font = mediumFont;
        tableLabelMediumStyle.fontColor = null;
        tableLabelSmallStyle = new LabelStyle(skin.get(LabelStyle.class));
        tableLabelSmallStyle.font = smallFont;
        tableLabelSmallStyle.fontColor = null;

        lineSpacing = properties.getScaledFloatProperty(PropertyName.CONSOLE_LINE_SPACING.name());
    }

    public void addWallOfText(TextHolder textHolder) {
        for (TextLine textLine : textHolder.getLines()) {
            addTextLine(textLine);
        }
    }

    public void addTextLine(final TextLine textLine) {

        String text = textLine.getText();

        // if we're at the line limit, remove the line on top of the list and
        // re-use the label object
        Label label = null;
        if (textLines.size() >= maximumTextLines) {
            label = textLines.poll();

            // get the cell for this label. we need to remove it to make space
            // for another cell
            Cell<?> labelCell = this.getCell(label);
            label.remove();

            // remove the label's cell from the table
            if (labelCell != null) {
                labelCell.setActor(null);
                this.getCells().removeValue(labelCell, true);

                // old gdx-1.0 logic
                // labelCell.setWidget(null);
                // this.getCells().remove(labelCell);
            } else {
                Gdx.app.error(SharedConstants.LOGGING_TAG_VIEW, "unable to find and remove cell for label");
                Gdx.app.error(SharedConstants.LOGGING_TAG_VIEW, "number of cells in this table = " + this.getCells().size);
            }

            // reuse the label
            label.setText(text);
        }

        if (label == null) {
            label = new Label(text, tableLabelStyle);
            label.setWrap(true);
        }

        if (textLine.getHyperlink() != null) {
            label.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Gdx.net.openURI(textLine.getHyperlink());
                }
            });
        }

        // Cell<?> row =  this.add(label).width(this.getWidth()).expandX().padBottom(10);
        Cell<?> row = this.add(label).fillX().expandX().padBottom(lineSpacing);

        if (textLine.indentSize() > 0) {
            row = row.padLeft(5 * textLine.indentSize());
        }

        if ("_".equals(text)) {
            row = row.padTop(lineSpacing);
        }
        if (TextStyleDecor.INVISIBLE == textLine.getDecor()) {
            label.setVisible(false);
        }
        if (TextStyleSize.LARGE == textLine.getSize()) {
            label.setStyle(tableLabelLargeStyle);
        }
        if (TextStyleSize.MEDIUM == textLine.getSize()) {
            label.setStyle(tableLabelStyle);
        }
        if (TextStyleSize.SMALL == textLine.getSize()) {
            label.setStyle(tableLabelSmallStyle);
        }
        if (textLine.getSize() == null || (TextStyleSize.STANDARD == textLine.getSize())) {
            label.setStyle(tableLabelStyle);
        }

        if (textLine.getColor() != null) {
            label.setColor(textLine.getColor().getColor());
        }

        TextStyleAlign alignment = textLine.getAlign();
        switch (alignment) {
            case LEFT:
                row.left();
                break;
            case RIGHT:
                row.right();
                break;
            case CENTERED:
                row.center();
                break;
            default:
                row.left();
                break;
        }
        this.row();
        this.pack();

        textLines.add(label);
        lineIndex++;
    }

    public int getLineIndex() {
        return lineIndex;
    }

    public void resize(int width, int height) {
    }

    /**
     * Clears all the lines from this table
     */
    public void clearText() {

        this.clear();
        textLines.clear();
    }

}
