/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

/**
 *   Have separate styles for different classes in the Skin
 *
 */
public enum SkinClass {

    SMALL_LABEL(LabelStyle.class),
    MEDIUM_LABEL(LabelStyle.class),
    LARGE_LABEL(LabelStyle.class),
    XS_LABEL(LabelStyle.class),
    XL_LABEL(LabelStyle.class),
    XXL_LABEL(LabelStyle.class),
    STANDARD_BUTTON(TextButtonStyle.class),
    LARGE_BUTTON(TextButtonStyle.class),
    XL_BUTTON(TextButtonStyle.class),
    MEDIUM_BUTTON(TextButtonStyle.class);

    private Class<?> styleClass;

    SkinClass(Class<?> styleClass) {
        this.styleClass = styleClass;
    }

    public Class<?> getStyleClass() {
        return styleClass;
    }

}
