/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.dodecahelix.libgdx.template.math.twodim.MovablePoint;

/**
 * The tiled background to the map
 *
 * @author dpeters
 *
 */
public class TiledBackground extends Actor {

    private TextureRegion backgroundTile;

    private float screenWidth;
    private float screenHeight;
    private float tileSize;

    // TODO - Update centerpoint externally
    /**
     *   This is the centerpoint of reference for the tile.  Tile can give the appearance of movement.
     *     This should be updated from the model
     */
    private MovablePoint centerpoint;

    public TiledBackground(float screenWidth, float screenHeight, TextureRegion backgroundTile) {

        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.backgroundTile = backgroundTile;
        tileSize = backgroundTile.getRegionHeight();

        this.setColor(Color.WHITE);

        centerpoint = new MovablePoint(256, 256);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        // stagger the tiles off the centerpoint of the screen
        float staggerX = centerpoint.getX() % tileSize;
        float staggerY = centerpoint.getY() % tileSize;

        Color originalColor = batch.getColor();
        batch.setColor(this.getColor());
        for (float x = -staggerX; x < screenWidth; x += tileSize) {
            for (float y = -staggerY; y < screenHeight; y += tileSize) {
                batch.draw(backgroundTile, x, y, tileSize, tileSize);
            }
        }
        batch.setColor(originalColor);
    }

    public void updateCenterpoint(float x, float y) {
        centerpoint.setX(x);
        centerpoint.setY(y);
    }

}
