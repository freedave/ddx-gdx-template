/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.view.resource.FontStyle;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;
import com.dodecahelix.libgdx.template.view.resource.TextureKey;

/**
 *   Configure the skin in java code rather than awkward skin file format  
 *
 *
 */
public class JSkin extends Skin {

    private boolean initialized;

    private ResourceController resources;
    private Properties properties;

    public JSkin(Properties properties, FontStyle defaultFont) {
        super(Gdx.files.internal(properties.getStringProperty(PropertyName.SKIN_FILE_PATH.name())));

        this.properties = properties;
        initialized = false;
    }

    public void init(ResourceController resources) {

        this.resources = resources;

        String defaultFontStyleName = properties.getStringProperty(PropertyName.UI_DEFAULT_FONT.name());
        FontStyle defaultFontStyle = FontStyle.getByName(defaultFontStyleName);

        String labelFontColorName = properties.getStringProperty(PropertyName.UI_LABEL_FONT_COLOR.name());
        Color labelFontColor = RgbColor.valueOf(labelFontColorName).getColor();

        int fontSizeStandard = properties.getScaledIntProperty(PropertyName.UI_STANDARD_FONT_SIZE.name());
        int fontSizeLarge = properties.getScaledIntProperty(PropertyName.UI_LARGE_FONT_SIZE.name());
        int fontSizeMedium = properties.getScaledIntProperty(PropertyName.UI_MEDIUM_FONT_SIZE.name());
        int fontSizeSmall = properties.getScaledIntProperty(PropertyName.UI_SMALL_FONT_SIZE.name());
        int fontSizeXS = properties.getScaledIntProperty(PropertyName.UI_XS_FONT_SIZE.name());
        int fontSizeXL = properties.getScaledIntProperty(PropertyName.UI_XL_FONT_SIZE.name());
        int fontSizeXXL = properties.getScaledIntProperty(PropertyName.UI_XXL_FONT_SIZE.name());

        BitmapFont standardFont = resources.findFont(defaultFontStyle, fontSizeStandard);
        BitmapFont largeFont = resources.findFont(defaultFontStyle, fontSizeLarge);
        BitmapFont mediumFont = resources.findFont(defaultFontStyle, fontSizeMedium);
        BitmapFont smallFont = resources.findFont(defaultFontStyle, fontSizeSmall);
        BitmapFont xsFont = resources.findFont(defaultFontStyle, fontSizeXS);
        BitmapFont xlFont = resources.findFont(defaultFontStyle, fontSizeXL);
        BitmapFont xxlFont = resources.findFont(defaultFontStyle, fontSizeXXL);

        // define the standard style
        LabelStyle labelStyle = this.get(LabelStyle.class);
        labelStyle.font = standardFont;
        labelStyle.fontColor = labelFontColor;

        // Add different font-size label styles
        LabelStyle largeLabelStyle = new LabelStyle(labelStyle);
        largeLabelStyle.font = largeFont;
        LabelStyle mediumLabelStyle = new LabelStyle(labelStyle);
        mediumLabelStyle.font = mediumFont;
        LabelStyle smallLabelStyle = new LabelStyle(labelStyle);
        smallLabelStyle.font = smallFont;
        LabelStyle xsLabelStyle = new LabelStyle(labelStyle);
        xsLabelStyle.font = xsFont;
        LabelStyle xlLabelStyle = new LabelStyle(labelStyle);
        xlLabelStyle.font = xlFont;
        LabelStyle xxlLabelStyle = new LabelStyle(labelStyle);
        xxlLabelStyle.font = xxlFont;

        this.add(SkinClass.MEDIUM_LABEL.name(), mediumLabelStyle, LabelStyle.class);
        this.add(SkinClass.LARGE_LABEL.name(), largeLabelStyle, LabelStyle.class);
        this.add(SkinClass.SMALL_LABEL.name(), smallLabelStyle, LabelStyle.class);
        this.add(SkinClass.XS_LABEL.name(), xsLabelStyle, LabelStyle.class);
        this.add(SkinClass.XL_LABEL.name(), xlLabelStyle, LabelStyle.class);
        this.add(SkinClass.XXL_LABEL.name(), xxlLabelStyle, LabelStyle.class);

        // Buttons
        TextureKey buttonUpKey = properties.getTextureProperty(PropertyName.DEFAULT_BUTTON_UP_TEXTURE.name());
        TextureKey buttonDownKey = properties.getTextureProperty(PropertyName.DEFAULT_BUTTON_DOWN_TEXTURE.name());
        TextureKey buttonDisabledKey = properties.getTextureProperty(PropertyName.DEFAULT_BUTTON_DISABLED_TEXTURE.name());

        String buttonFontColorName = properties.getStringProperty(PropertyName.UI_BUTTON_FONT_COLOR.name());
        Color buttonFontColor = RgbColor.valueOf(buttonFontColorName).getColor();
        String buttonDisabledFontColorName = properties.getStringProperty(PropertyName.UI_BUTTON_DISABLED_FONT_COLOR.name());
        Color buttonDisabledFontColor = RgbColor.valueOf(buttonDisabledFontColorName).getColor();

        // TextButtons
        TextButtonStyle standardButtonStyle = this.get(TextButtonStyle.class);
        standardButtonStyle.up = getNinePatchDrawable(buttonUpKey);
        standardButtonStyle.down = getNinePatchDrawable(buttonDownKey);
        standardButtonStyle.disabled = getNinePatchDrawable(buttonDisabledKey);
        standardButtonStyle.disabledFontColor = buttonDisabledFontColor;
        standardButtonStyle.font = standardFont;
        standardButtonStyle.fontColor = buttonFontColor;

        TextButtonStyle mediumButtonStyle = new TextButtonStyle(standardButtonStyle);
        mediumButtonStyle.font = mediumFont;
        TextButtonStyle largeButtonStyle = new TextButtonStyle(standardButtonStyle);
        largeButtonStyle.font = largeFont;
        TextButtonStyle xlButtonStyle = new TextButtonStyle(standardButtonStyle);
        xlButtonStyle.font = xlFont;

        this.add(SkinClass.STANDARD_BUTTON.name(), standardButtonStyle, TextButtonStyle.class);
        this.add(SkinClass.LARGE_BUTTON.name(), largeButtonStyle, TextButtonStyle.class);
        this.add(SkinClass.MEDIUM_BUTTON.name(), mediumButtonStyle, TextButtonStyle.class);
        this.add(SkinClass.XL_BUTTON.name(), xlButtonStyle, TextButtonStyle.class);

        TextureKey scrollbarFrameKey = properties.getTextureProperty(PropertyName.DEFAULT_SCROLLBAR_FRAME_TEXTURE.name());
        TextureKey scrollbarBackgroundKey = properties.getTextureProperty(PropertyName.DEFAULT_SCROLLBAR_BG_TEXTURE.name());
        TextureKey scrollbarHandleKey = properties.getTextureProperty(PropertyName.DEFAULT_SCROLLBAR_HANDLE_TEXTURE.name());

        // Scroll Pane
        ScrollPaneStyle scrollerStyle = new ScrollPaneStyle();
        scrollerStyle.background = getNinePatchDrawable(scrollbarFrameKey);
        scrollerStyle.vScroll = getNinePatchDrawable(scrollbarBackgroundKey);
        scrollerStyle.vScrollKnob = new TextureRegionDrawable(resources.findTexture(scrollbarHandleKey));
        this.add("default", scrollerStyle);

        // Windows
        WindowStyle windowStyle = this.get(WindowStyle.class);
        windowStyle.background = getNinePatchDrawable(TextureKey.WINDOW_DEFAULT_BACKGROUND);
        windowStyle.titleFont = standardFont;
        windowStyle.titleFontColor = Color.BLACK;

        // lists
        TextureKey listBackground = properties.getTextureProperty(PropertyName.DEFAULT_LIST_BG_TEXTURE.name());
        ListStyle listStyle = this.get(ListStyle.class);
        listStyle.background = getNinePatchDrawable(listBackground);
        listStyle.font = largeFont;
        listStyle.fontColorSelected = RgbColor.RED_ORANGERED.getColor();
        listStyle.fontColorUnselected = RgbColor.BLACK.getColor();

        // ComboBoxes
        TextureKey sbBackground = properties.getTextureProperty(PropertyName.DEFAULT_SELECTBOX_TEXTURE.name());
        SelectBoxStyle sbStyle = this.get(SelectBoxStyle.class);
        sbStyle.background = getNinePatchDrawable(sbBackground);
        sbStyle.listStyle = listStyle;
        sbStyle.scrollStyle = scrollerStyle;
        sbStyle.font = largeFont;
        sbStyle.fontColor = Color.BLACK;

        initialized = true;
    }

    private NinePatchDrawable getNinePatchDrawable(TextureKey key) {
        return new NinePatchDrawable(resources.findNinePatch(key));
    }

    public boolean isInitialized() {
        return initialized;
    }

}
