/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.ui;

import com.badlogic.gdx.graphics.Color;

public enum RgbColor {

    BLACK("000000"),
    WHITE("FFFFFF"),
    BLUE_AQUA("00FFFF"),
    BLUE_BLUEVIOLET("8A2BE2"),
    RED_CRIMSON("DC143C"),
    BLUE_DARKCYAN("008B8B"),
    GREEN_DARKSEAGREEN("8FBC8F"),
    BLUE_DEEPSKYBLUE("00BFFF"),
    BLUE_DODGERBLUE("1E90FF"),
    GREEN_FORESTGREEN("228B22"),
    YELLOW_GOLD("FFD700"),
    YELLOW_GOLDENROD("DAA520"),
    GREEN_GREENYELLOW("ADFF2F"),
    RED_INDIANRED("CD5C5C"),
    BLUE_INDIGO("4B0082"),
    YELLOW_KHAKI("F0E68C"),
    GREEN_LIGHTGREEN("90EE90"),
    RED_LIGHTCORAL("F08080"),
    RED_LIGHTSALMON("FFA07A"),
    BLUE_LIGHTSEAGREEN("20B2AA"),
    BLUE_LIGHTSTEELBLUE("B0C4DE"),
    GREEN_LIMEGREEN("32CD32"),
    RED_MAROON("800000"),
    PURPLE_MEDIUMORCHID("BA55D3"),
    GREEN_MEDIUMSEAGREEN("3CB371"),
    BLUE_MIDNIGHTBLUE("191970"),
    GREEN_OLIVEDRAB("6B8E23"),
    ORANGE("FFA500"),
    RED_ORANGERED("FF4500"),
    BROWN_SADDLEBROWN("8B4513"),
    GREEN_SEAGREEN("2E8B57"),
    BLUE_SLATEBLUE("6A5ACD"),
    GREY_SLATEGREY("708090"),
    GREEN_SPRINGGREEN("00FF7F"),
    BLUE_STEELBLUE("4682B4"),
    BLUE_TEAL("008080"),
    RED_TOMATO("FF6347"),
    GREEN_YELLOWGREEN("9ACD32"),
    DARK_PURPLE("330033"),;

    private String hexCode;

    RgbColor(String hexCode) {
        this.hexCode = hexCode;
    }

    public Color getColor() {
        return Color.valueOf(hexCode + "FF");
    }

    public String getHexCode() {
        return hexCode;
    }

}
