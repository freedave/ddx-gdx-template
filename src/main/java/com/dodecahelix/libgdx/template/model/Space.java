/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

import com.badlogic.gdx.utils.LongArray;
import com.badlogic.gdx.utils.LongMap.Values;
import com.badlogic.gdx.utils.Pool;
import com.dodecahelix.libgdx.template.control.events.ViewEvent;

/**
 *   Represents a group of similar entities (of the same type)
 *
 */
public abstract class Space<T extends Entity> {

    // amount of time passed (in seconds) while this state has been active
    protected float timePassed = 0;

    private SpaceId id;
    private SpaceState state;

    /**
     *   Maintain a pool of entities to reduce garbage
     */
    private Pool<T> entityPool;

    /**
     *  Maps the entity to its ID
     */
    private EntityMap<T> entities;

    public Space(SpaceId id, int entityCapacity) {
        entities = new EntityMap<T>(entityCapacity);
        this.id = id;
        this.state = SpaceState.CREATED;

        entityPool = new Pool<T>(entityCapacity) {
            @Override
            protected T newObject() {
                //return Entity.buildVoid(Space.this);
                return buildNewEntity();
            }
        };
    }


    protected abstract T buildNewEntity();

    /**
     *   Set/Reset the entire Grid model back to its original state.
     *   (make sure data is clean first, since this can be called both on initialization and reset button)
     */
    public abstract void reset();


    public void addEntity(T entity) {
        entities.addEntity(entity);
    }

    /**
     *
     * @param entityType
     * @param id
     * @return
     */
    public Entity lookupEntity(long id) {
        return entities.lookupEntity(id);
    }

    /**
     *   Returns all of the entities in this space
     *
     * @return
     */
    public Values<T> getAllEntities() {
        return entities.getAllEntities();
    }

    public LongArray getEntitiesByLifecycleState(EntityLifecycleState lifecycleState) {
        return entities.getEntitiesByLifecycleState(lifecycleState);
    }

    /**
     *   All lifecycle changes are managed by the entity map
     *
     * @param entityId
     * @param newState
     */
    public void updateEntityLifecycleState(long entityId, EntityLifecycleState newState) {
        entities.updateEntityLifecycleState(entityId, newState);
    }


    public void updateEntityState(long entityId, EntityState newState) {
        entities.updateEntityState(entityId, newState);
    }

    /**
     * Removes all entities
     */
    public void clear() {
        entities.clear();

        this.timePassed = 0;
    }

    public void removeEntity(long id) {
        T removedEntity = entities.removeEntity(id);
        entityPool.free(removedEntity);
    }

    public SpaceId getId() {
        return id;
    }

    public SpaceType getType() {
        return id.getType();
    }

    public SpaceState getState() {
        return state;
    }

    public void setState(SpaceState state) {
        this.state = state;
    }

    public Pool<T> getEntityPool() {
        return entityPool;
    }

    public void bumpSpace(float delta) {
        // keep track of time spent in the active state - for running of timed jobs (exery X seconds)
        timePassed += delta;

        // make sure no time overflow - not gunna happen, but just to be safe
        if (timePassed > 999999.0f) {
            timePassed = 0;
        }

        bump(delta);
    }

    protected abstract void bump(float delta);

    public abstract void handleEvent(ViewEvent event);

    /**
     *   Called when the Screen/View changes, all associated spaces are activates
     *
     * @param active
     */
    public void setActive(boolean active) {
        if (active) {
            this.setState(SpaceState.ACTIVE);

            // unpause entities
            LongArray pausedEntities = entities.getEntitiesByLifecycleState(EntityLifecycleState.PAUSED);
            for (int i = 0; i < pausedEntities.size; i++) {
                long entityId = pausedEntities.get(i);
                entities.updateEntityLifecycleState(entityId, EntityLifecycleState.INITIALIZED);
            }
        } else {
            this.setState(SpaceState.PAUSED);

            // pause active entities
            LongArray activeEntities = entities.getEntitiesByLifecycleState(EntityLifecycleState.ACTIVE_SYNCHRONIZED);
            for (int i = 0; i < activeEntities.size; i++) {
                long entityId = activeEntities.get(i);
                entities.updateEntityLifecycleState(entityId, EntityLifecycleState.PAUSED);
            }

            LongArray activDirtyeEntities = entities.getEntitiesByLifecycleState(EntityLifecycleState.ACTIVE_DIRTY);
            for (int i = 0; i < activDirtyeEntities.size; i++) {
                long entityId = activDirtyeEntities.get(i);
                entities.updateEntityLifecycleState(entityId, EntityLifecycleState.PAUSED);
            }
        }

    }

}
