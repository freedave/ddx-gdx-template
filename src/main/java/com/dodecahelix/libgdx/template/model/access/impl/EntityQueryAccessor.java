/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.access.impl;

import com.badlogic.gdx.utils.Array;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.model.Space;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.SpaceState;
import com.dodecahelix.libgdx.template.model.access.ReadWriteAccessor;


/**
 *   Finds entities in the model
 *
 * @author dpeters
 *
 */
public class EntityQueryAccessor implements ReadWriteAccessor {

    Model model;

    /**
     *   Reuse the queryResult object
     */
    private Array<Entity> queryResult;

    public EntityQueryAccessor(Model model, Properties properties) {
        this.model = model;

        queryResult = new Array<Entity>(properties.getIntProperty(PropertyName.ENTITY_CAPACITY.name()));
    }

    /**
     *   Returns all entities in the model
     */
    public Array<Entity> getAllEntitiesInActiveSpaces() {
        queryResult.clear();

        for (SpaceId spaceId : model.spaces()) {
            Space<? extends Entity> space = model.getSpace(spaceId);
            if (SpaceState.ACTIVE == space.getState()) {
                for (Entity entity : space.getAllEntities()) {
                    queryResult.add(entity);
                }
            }
        }

        return queryResult;
    }

    /**
     *   Search through all spaces to find the given entity
     *
     * @param entityType
     * @param id
     */
    public Entity findEntityInActiveSpaces(long id) {
        Entity entity = null;
        for (SpaceId spaceId : model.spaces()) {
            Space<? extends Entity> space = model.getSpace(spaceId);
            if (SpaceState.ACTIVE == space.getState()) {
                entity = space.lookupEntity(id);
                if (entity != null) {
                    return entity;
                }
            }
        }
        return null;
    }

}
