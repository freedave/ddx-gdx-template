/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.interfaces;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.text.TextLine;

public class ConsoleEntity extends UiEntity {

    /**
     *   New text lines to be added to the console - model does not need to track old lines.  View (actor) can do that.
     */
    private List<TextLine> newLines = new ArrayList<TextLine>();

    public ConsoleEntity(SpaceId space) {
        super(space);
    }

    @Override
    public void synchronizeStaticFields(Entity modelEntity) {
        if (!modelEntity.isSynched()) {
            // we only need to cast when its out of sync
            ConsoleEntity modelConsole = (ConsoleEntity) modelEntity;
            this.setNewLines(modelConsole.getNewLines());
            modelEntity.setSynched(true);

            // now the view will be out of sync with its own model. use this to tell it to refresh
            this.setSynched(false);
        }
    }

    public List<TextLine> getNewLines() {
        return newLines;
    }

    public void setNewLines(List<TextLine> newLines) {
        this.newLines = newLines;
    }

    public void addNewLine(TextLine newLine) {
        this.newLines.add(newLine);
    }
}
