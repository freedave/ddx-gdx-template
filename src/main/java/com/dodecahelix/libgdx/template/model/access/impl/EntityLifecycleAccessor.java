/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.access.impl;

import com.badlogic.gdx.utils.LongArray;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.EntityLifecycleState;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.model.Space;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.SpaceState;

public class EntityLifecycleAccessor {

    Model model;

    private LongArray queryResult;

    public EntityLifecycleAccessor(Model model, Properties properties) {
        this.model = model;

        queryResult = new LongArray(properties.getIntProperty(PropertyName.ENTITY_CAPACITY.name()));
    }

    public LongArray getInitializedEntities() {
        return getEntitiesByLifecycleState(EntityLifecycleState.INITIALIZED);
    }

    public LongArray getDestroyedEntities() {
        return getEntitiesByLifecycleState(EntityLifecycleState.DESTROYED);
    }

    private LongArray getEntitiesByLifecycleState(EntityLifecycleState lifecycleState) {
        queryResult.clear();

        for (SpaceId spaceId : model.spaces()) {
            Space<? extends Entity> space = model.getSpace(spaceId);
            if (SpaceState.ACTIVE == space.getState()) {
                LongArray entities = space.getEntitiesByLifecycleState(lifecycleState);
                for (int i = 0; i < entities.size; i++) {
                    long entityId = entities.get(i);
                    queryResult.add(entityId);
                }
            }
        }

        return queryResult;
    }

    public void activateInitializedEntities() {
        for (SpaceId spaceId : model.spaces()) {
            Space<? extends Entity> space = model.getSpace(spaceId);
            if (SpaceState.ACTIVE == space.getState()) {
                LongArray entities = space.getEntitiesByLifecycleState(EntityLifecycleState.INITIALIZED);

                // since you are removing entities, the size will decrease
                while (entities.size > 0) {
                    space.updateEntityLifecycleState(entities.get(0), EntityLifecycleState.ACTIVE_SYNCHRONIZED);
                }
            }
        }
    }
}
