/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.twodim;

import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.model.Space;
import com.dodecahelix.libgdx.template.model.SpaceId;

public abstract class Grid extends Space<GridEntity> {

    /**
     *   Whether or not to wrap entities that fall out of bounds.  Otherwise, they will be destroyed.
     */
    private boolean wrap = true;

    public Grid(SpaceId id, int entityCapacity) {
        super(id, entityCapacity);
    }

    public boolean isWrap() {
        return wrap;
    }

    public void setWrap(boolean wrap) {
        this.wrap = wrap;
    }

    /**
     *  GridBounds is the full scope of the Grid in 2d cartesian coordinate system
     *
     * @return the coordinates of the bottom left and top right in the cartesian 2D coordinate system
     */
    public abstract Bounds getGridBounds();

    /**
     *   Viewport is the section of the Grid that is visible on the screen.
     *
     * @return bounds for the viewport
     */
    public abstract Bounds getViewportBounds();
}
