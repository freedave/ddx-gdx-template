/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.access.bump;

import com.dodecahelix.libgdx.template.math.twodim.collision.EntityCollisionAccessor;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.model.access.impl.EntityMotionAccessor;

public class GridBumpAccessor extends BumpAccessor {

    EntityMotionAccessor entityMotionAccessor;

    EntityCollisionAccessor entityCollisionAccessor;

    public GridBumpAccessor(Model model,
                            EntityMotionAccessor entityMotionAccessor,
                            EntityCollisionAccessor entityCollisionAccessor) {

        super(model);
        this.entityMotionAccessor = entityMotionAccessor;
        this.entityCollisionAccessor = entityCollisionAccessor;
    }

    @Override
    public void bump(float delta) {

        // update position and rotation of active entities
        entityMotionAccessor.updateEntityMotion(delta);

        // check for collisions and update speed and rotations for collisions
        // get all grids in the universe, and get viewports
        entityCollisionAccessor.resolveAllActiveGridCollisionsByViewport();
    }

}
