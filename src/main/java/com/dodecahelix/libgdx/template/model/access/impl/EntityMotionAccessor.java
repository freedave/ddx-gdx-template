/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.access.impl;

import com.dodecahelix.libgdx.template.math.twodim.BoundaryWrapper;
import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.math.twodim.MotionUtil;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.EntityState;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.model.attributes.AttributeHolder;
import com.dodecahelix.libgdx.template.model.twodim.Grid;
import com.dodecahelix.libgdx.template.types.Attribute.StandardAttribute;

public class EntityMotionAccessor {

    Model model;

    EntityDeletionAccessor deletionAccessor;

    public EntityMotionAccessor(Model model, EntityDeletionAccessor deletionAccessor) {
        super();
        this.model = model;
        this.deletionAccessor = deletionAccessor;
    }

    public void updateEntityMotion(float delta) {
        // TODO - figure out if updates needed for other space types (i.e; universe, chessboard)

        // update grids
        updateGridEntityMotions(delta);
    }

    private void updateGridEntityMotions(float delta) {
        for (Grid grid : model.getAllSpaces().getGrids()) {
            // TODO - check state for grid
            for (Entity entity : grid.getAllEntities()) {

                // Update entity motions only if the state is active, invisible or phantom
                if (EntityState.ACTIVE == entity.getState() ||
                EntityState.PHANTOM == entity.getState() ||
                EntityState.INVISIBLE == entity.getState()) {

                    AttributeHolder holder = entity.getAttributes();
                    float xPos = holder.getFloatValue(StandardAttribute.POSITION_X.getId(), 0);
                    float yPos = holder.getFloatValue(StandardAttribute.POSITION_Y.getId(), 0);
                    float direction = holder.getFloatValue(StandardAttribute.MOVEMENT_ANGLE.getId(), 0);
                    float speed = holder.getFloatValue(StandardAttribute.SPEED.getId(), 0);

                    xPos = MotionUtil.updatePositionXDir(xPos, direction, speed, delta);
                    yPos = MotionUtil.updatePositionYDir(yPos, direction, speed, delta);

                    Bounds bounds = grid.getGridBounds();
                    // wrap?
                    if (grid.isWrap()) {
                        xPos = BoundaryWrapper.wrapPositionX(xPos, bounds);
                        yPos = BoundaryWrapper.wrapPositionY(yPos, bounds);
                    } else {
                        if (!bounds.containsPoint(xPos, yPos)) {
                            deletionAccessor.addEntityToDeletionQueue(entity.getId());
                        }
                    }

                    holder.setFloatValue(StandardAttribute.POSITION_X.getId(), xPos);
                    holder.setFloatValue(StandardAttribute.POSITION_Y.getId(), yPos);
                }

                if (EntityState.BACKGROUND == entity.getState()) {
                    // Background entity should map to viewport centerpoint
                    AttributeHolder holder = entity.getAttributes();

                    float xPos = grid.getViewportBounds().getCenterpoint().getX();
                    float yPos = grid.getViewportBounds().getCenterpoint().getY();
                    holder.setFloatValue(StandardAttribute.POSITION_X.getId(), xPos);
                    holder.setFloatValue(StandardAttribute.POSITION_Y.getId(), yPos);
                }
            }

            deletionAccessor.deleteQueuedEntities();
        }
    }

}
