/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

public enum SpaceType {
    GRID,                // standard 2d top-down space with pixel-sized coordinates
    UNIVERSE,            // 3d version of grid
    CHESSBOARD,            // 2d coordinate system, broken down into large-block coordinates
    TILEMAP,            // 2d coordinate system, broken down into mid-sized block populated by tiles (background, and interactive)
    SIDESCROLLER,        // coordinate system represnting one side of a 3d universe
    DIMENSIONLESS,        // container for entities without dimensional information
    USER_INTERFACE;      // space that hold user interface entities (using a layout)

    public static final SpaceType[] VALUES = SpaceType.values();
}
