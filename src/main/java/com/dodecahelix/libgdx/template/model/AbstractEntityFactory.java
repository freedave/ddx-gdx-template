/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;

public abstract class AbstractEntityFactory<T extends Entity> implements EntityFactory<T> {

    Properties properties;

    public AbstractEntityFactory(Properties properties) {
        super();
        this.properties = properties;
    }

    protected abstract T obtainEntity(int entityTypeId, Space<T> space);

    @Override
    public T createNewEntity(int entityTypeId, Space<T> space) {
        T entity = obtainEntity(entityTypeId, space);

        if (entity != null) {
            if (properties.getBooleanProperty(PropertyName.LOG_ENTITY_CREATION.name())) {
                Gdx.app.debug(SharedConstants.LOGGING_TAG_ENTITY_CREATION, "Entity Created : " + entity);
            }
            space.addEntity(entity);
        } else {
            Gdx.app.debug(SharedConstants.LOGGING_TAG_ENTITY_CREATION, "Could not create entity, type id unknown : " + entityTypeId);
        }

        return entity;
    }

}
