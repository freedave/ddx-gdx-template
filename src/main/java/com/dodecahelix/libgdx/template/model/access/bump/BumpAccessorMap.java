/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.access.bump;

import com.dodecahelix.libgdx.template.model.SpaceType;

public class BumpAccessorMap {

    GridBumpAccessor gridBumpAccessor;

    UniverseBumpAccessor universeBumpAccessor;

    ChessboardBumpAccessor chessboardBumpAccessor;

    SidescrollerBumpAccessor sidescrollerBumpAccessor;

    TilemapBumpAccessor tilemapBumpAccessor;

    UserInterfaceBumpAccessor userInterfaceBumpAccessor;

    DimensionlessBumpAccessor dimensionlessBumpAccessor;

    public BumpAccessorMap(GridBumpAccessor gridBumpAccessor,
                           UniverseBumpAccessor universeBumpAccessor,
                           ChessboardBumpAccessor chessboardBumpAccessor,
                           SidescrollerBumpAccessor sidescrollerBumpAccessor,
                           TilemapBumpAccessor tilemapBumpAccessor,
                           UserInterfaceBumpAccessor userInterfaceBumpAccessor,
                           DimensionlessBumpAccessor dimensionlessBumpAccessor) {

        super();
        this.gridBumpAccessor = gridBumpAccessor;
        this.universeBumpAccessor = universeBumpAccessor;
        this.chessboardBumpAccessor = chessboardBumpAccessor;
        this.sidescrollerBumpAccessor = sidescrollerBumpAccessor;
        this.tilemapBumpAccessor = tilemapBumpAccessor;
        this.userInterfaceBumpAccessor = userInterfaceBumpAccessor;
        this.dimensionlessBumpAccessor = dimensionlessBumpAccessor;
    }


    public BumpAccessor getBumpAccessorForType(SpaceType spaceType) {
        switch (spaceType) {
            case GRID:
                return gridBumpAccessor;
            case UNIVERSE:
                return universeBumpAccessor;
            case CHESSBOARD:
                return chessboardBumpAccessor;
            case SIDESCROLLER:
                return sidescrollerBumpAccessor;
            case TILEMAP:
                return tilemapBumpAccessor;
            case USER_INTERFACE:
                return userInterfaceBumpAccessor;
            case DIMENSIONLESS:
                return dimensionlessBumpAccessor;
        }

        return null;
    }


}
