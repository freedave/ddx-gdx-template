/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.access.impl;

import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.model.Space;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.SpaceState;

public class SpaceStateAccessor {

    Model model;

    public SpaceStateAccessor(Model model) {
        super();
        this.model = model;
    }

    /**
     *   Sets the state of the given Spaces to active.  Sets all other states to paused.
     * @param associatedSpaces
     */
    public void spaceActivation(Set<SpaceId> associatedSpaces) {

        for (SpaceId spaceId : model.spaces()) {
            Space<? extends Entity> space = model.getSpace(spaceId);
            if (associatedSpaces.contains(spaceId)) {
                Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Activating space " + spaceId);
                space.setActive(true);
            } else {
                if (space.getState() == SpaceState.ACTIVE) {
                    space.setState(SpaceState.PAUSED);
                    Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Pausing space " + spaceId);
                    space.setActive(false);
                }
            }
        }
    }

}
