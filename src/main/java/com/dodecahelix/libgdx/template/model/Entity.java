/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

import com.badlogic.gdx.utils.Pool.Poolable;
import com.dodecahelix.libgdx.template.model.attributes.AttributeHolder;
import com.dodecahelix.libgdx.template.util.UniqueIdCounter;

/**
 *    Represents a model object
 */
public abstract class Entity implements Poolable {

    private EntityState state;
    private EntityLifecycleState lifecycleState;

    /*
     *  optional flag - use this flag to indicate that an Entity has changed in the model and is out of sync with the client
     */
    private boolean synched;

    private int entityType;
    private long id;

    private SpaceId spaceId;

    private AttributeHolder attributes;

    public Entity(SpaceId space) {
        this.entityType = 0;

        // default state is active
        this.state = EntityState.INACTIVE;
        this.lifecycleState = EntityLifecycleState.INITIALIZED;
        this.id = UniqueIdCounter.getInstance().generateNewId();
        this.spaceId = space;

        attributes = new AttributeHolder();
    }

    public void setup(int entityTypeCode, EntityState state) {
        this.entityType = entityTypeCode;
        this.state = state;
        this.lifecycleState = EntityLifecycleState.INITIALIZED;
    }

    /**
     *   Use this to synchronize statically typed model fields with the entity clone (part of EntityActor)
     *
     *   (This avoids the casting when using the EntitySynchController.  Optional)
     *
     * @param modelEntity
     */
    public abstract void synchronizeStaticFields(Entity modelEntity);

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public void reset() {
        this.state = EntityState.INACTIVE;
        this.lifecycleState = EntityLifecycleState.DESTROYED;
        this.id = UniqueIdCounter.getInstance().generateNewId();
    }

    public EntityState getState() {
        return state;
    }

    protected void setState(EntityState state) {
        this.state = state;
    }

    public int getEntityType() {
        return entityType;
    }

    public long getId() {
        return id;
    }

    public AttributeHolder getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributeHolder attributes) {
        this.attributes = attributes;
    }

    public SpaceId getSpaceId() {
        return spaceId;
    }

    public EntityLifecycleState getLifecycleState() {
        return lifecycleState;
    }

    protected void setLifecycleState(EntityLifecycleState lifecycleState) {
        this.lifecycleState = lifecycleState;
    }

    public boolean isSynched() {
        return synched;
    }

    public void setSynched(boolean synched) {
        this.synched = synched;
    }

    @Override
    public String toString() {
        return "Entity [state=" + state + ", lifecycleState=" + lifecycleState + ", type=" + entityType + ", id=" + id + ", spaceId=" + spaceId + ", attributes="
        + attributes + "]";
    }


}
