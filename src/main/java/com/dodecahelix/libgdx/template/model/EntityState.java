/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

public enum EntityState {

    ACTIVE,       // Entity is added to the screen, and can collide, move accept hits and controls
    PHANTOM,      // Entity is visible on screen, moving, but does not accept hits or controls
    INVISIBLE,    // Entity is not visible on screen, but moving and accepts hits
    PAUSED,       // Entity is visible on screen, but not moving and does not accept hits or controls
    INACTIVE,     // Entity is not visible, not moving and does not accept hits or controls
    BACKGROUND,   // Entity is static, fixed on the background, moving with the viewport
    ;
}
