/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.LongArray;

public class EntityStateMap {

    public IntMap<LongArray> states;
    public IntMap<LongArray> lifecycleStates;

    public EntityStateMap() {
        states = new IntMap<LongArray>();
        lifecycleStates = new IntMap<LongArray>();

        clear();
    }

    public LongArray getEntitiesByState(EntityState state) {
        return states.get(state.ordinal());
    }

    public LongArray getEntitiesByLifecycleState(EntityLifecycleState state) {
        return lifecycleStates.get(state.ordinal());
    }

    public void updateLifecycleState(Entity entity, EntityLifecycleState newState) {
        lifecycleStates.get(entity.getLifecycleState().ordinal()).removeValue(entity.getId());
        lifecycleStates.get(newState.ordinal()).add(entity.getId());
    }

    public void updateState(Entity entity, EntityState newState) {
        states.get(entity.getState().ordinal()).removeValue(entity.getId());
        states.get(newState.ordinal()).add(entity.getId());
    }

    public void addEntity(Entity entity) {
        lifecycleStates.get(entity.getLifecycleState().ordinal()).add(entity.getId());
        states.get(entity.getState().ordinal()).add(entity.getId());
    }

    public void removeEntity(Entity entity) {
        lifecycleStates.get(entity.getLifecycleState().ordinal()).removeValue(entity.getId());
        states.get(entity.getState().ordinal()).removeValue(entity.getId());
    }

    public void clear() {
        lifecycleStates.clear();
        states.clear();

        for (EntityState state : EntityState.values()) {
            states.put(state.ordinal(), new LongArray());
        }

        for (EntityLifecycleState state : EntityLifecycleState.values()) {
            lifecycleStates.put(state.ordinal(), new LongArray());
        }
    }

}
