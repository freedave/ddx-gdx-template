/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.access;

import com.dodecahelix.libgdx.template.math.twodim.collision.EntityCollisionAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityDeletionAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityLifecycleAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityMotionAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityQueryAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.ModelLoadingAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.SpaceEventAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.SpaceStateAccessor;

/**
 *   Contains all the API's that can both read and write data to the model.
 *
 *   Used to distinguish which API's cannot be exposed directly to the view
 *
 * @author dpeters
 *
 */
public class ReadWriteAccessorMap {

    ModelLoadingAccessor modelLoadingAccessor;

    EntityQueryAccessor entityQueryAccessor;

    EntityMotionAccessor entityMotionAccessor;

    EntityDeletionAccessor entityDeletionAccessor;

    EntityCollisionAccessor entityCollisionAccessor;

    SpaceStateAccessor spaceStateAccessor;

    EntityLifecycleAccessor entityLifecycleAccessor;

    SpaceEventAccessor spaceActionAccessor;

    public ReadWriteAccessorMap(ModelLoadingAccessor modelLoadingAccessor,
                                EntityQueryAccessor entityQueryAccessor,
                                EntityMotionAccessor entityMotionAccessor,
                                EntityDeletionAccessor entityDeletionAccessor,
                                EntityCollisionAccessor entityCollisionAccessor,
                                SpaceStateAccessor spaceStateAccessor,
                                EntityLifecycleAccessor entityLifecycleAccessor,
                                SpaceEventAccessor spaceActionAccessor) {

        super();
        this.modelLoadingAccessor = modelLoadingAccessor;
        this.entityQueryAccessor = entityQueryAccessor;
        this.entityMotionAccessor = entityMotionAccessor;
        this.entityDeletionAccessor = entityDeletionAccessor;
        this.entityCollisionAccessor = entityCollisionAccessor;
        this.spaceStateAccessor = spaceStateAccessor;
        this.entityLifecycleAccessor = entityLifecycleAccessor;
        this.spaceActionAccessor = spaceActionAccessor;
    }

    public ModelLoadingAccessor getModelLoadingAccessor() {
        return modelLoadingAccessor;
    }

    public EntityQueryAccessor getEntityQueryAccessor() {
        return entityQueryAccessor;
    }

    public EntityMotionAccessor getEntityMotionAccessor() {
        return entityMotionAccessor;
    }

    public EntityDeletionAccessor getEntityDeletionAccessor() {
        return entityDeletionAccessor;
    }

    public EntityCollisionAccessor getEntityCollisionAccessor() {
        return entityCollisionAccessor;
    }

    public SpaceStateAccessor getSpaceStateAccessor() {
        return spaceStateAccessor;
    }

    public EntityLifecycleAccessor getEntityLifecycleAccessor() {
        return entityLifecycleAccessor;
    }

    public SpaceEventAccessor getSpaceActionAccessor() {
        return spaceActionAccessor;
    }

}
