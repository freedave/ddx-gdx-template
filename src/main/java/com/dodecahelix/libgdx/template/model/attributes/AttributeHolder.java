/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.attributes;

import com.badlogic.gdx.utils.BooleanArray;
import com.badlogic.gdx.utils.CharArray;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntIntMap;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.LongArray;
import com.dodecahelix.libgdx.template.config.SharedTypes;
import com.dodecahelix.libgdx.template.types.Attribute;

public class AttributeHolder implements HasAttributes {

    // maps the Attribute ID to its order in the associated array, for lookup in the array (key = attributeId, value = order)
    private IntIntMap attributeOrders;

    private BooleanArray booleanAttributes;
    private IntArray intAttributes;
    private FloatArray floatAttributes;
    private CharArray charAttributes;
    private LongArray longAttributes;

    private IntMap<String> stringAttributes;

    public AttributeHolder() {
        // try to keep the memory allocation small
        // Unfortunately, IntIntMap is buggy with 0 keys, so hack around it by adding one when putting a key in the map
        attributeOrders = new IntIntMap(25);

        booleanAttributes = new BooleanArray(true, 3);
        intAttributes = new IntArray(true, 3);
        floatAttributes = new FloatArray(true, 10);
        longAttributes = new LongArray(true, 3);
        charAttributes = new CharArray(true, 3);

        stringAttributes = new IntMap<String>(3);
    }

    /**
     *   Copy all attributes from another attribute holder
     *
     * @param copyFromHolder
     */
    public void copyFrom(AttributeHolder copyFromHolder) {

        for (Attribute attribute : SharedTypes.getInstance().getAttributes()) {
            AttributeType type = attribute.getType();
            switch (type) {
                case STRING:
                    this.setStringValue(attribute.getCode(), copyFromHolder.getStringValue(attribute.getCode()));
                    break;
                case INT:
                    this.setIntValue(attribute.getCode(), copyFromHolder.getIntValue(attribute.getCode(), 0));
                    break;
                case LONG:
                    this.setLongValue(attribute.getCode(), copyFromHolder.getLongValue(attribute.getCode(), 0));
                    break;
                case FLOAT:
                    this.setFloatValue(attribute.getCode(), copyFromHolder.getFloatValue(attribute.getCode(), 0));
                    break;
                case BOOLEAN:
                    this.setBooleanValue(attribute.getCode(), copyFromHolder.getBooleanValue(attribute.getCode(), false));
                    break;
                case CHAR:
                    this.setCharValue(attribute.getCode(), copyFromHolder.getCharValue(attribute.getCode(), ' '));
                    break;
            }
        }
    }

    @Override
    public int getIntValue(int attributeId, int defaultValue) {
        int value = defaultValue;
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            value = intAttributes.get(order);
        }
        return value;
    }

    @Override
    public float getFloatValue(int attributeId, float defaultValue) {
        float value = defaultValue;
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            value = floatAttributes.get(order);
        }
        return value;
    }

    @Override
    public String getStringValue(int attributeId) {
        return stringAttributes.get(attributeId);
    }

    @Override
    public boolean getBooleanValue(int attributeId, boolean defaultValue) {
        boolean value = defaultValue;
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            value = booleanAttributes.get(order);
        }
        return value;
    }

    @Override
    public long getLongValue(int attributeId, long defaultValue) {
        long value = defaultValue;
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            value = longAttributes.get(order);
        }
        return value;
    }

    @Override
    public char getCharValue(int attributeId, char defaultValue) {
        char value = 0;
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            value = charAttributes.get(order);
        }
        return value;
    }

    @Override
    public void setStringValue(int attributeId, String value) {
        stringAttributes.put(attributeId, value);
    }

    @Override
    public void setIntValue(int attributeId, int value) {
        // if the value exists, overwrite it, else add it to the end of the array
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            intAttributes.set(order, value);
        } else {
            attributeOrders.put(attributeId + 1, intAttributes.size);
            intAttributes.add(value);
        }
    }

    @Override
    public void setFloatValue(int attributeId, float value) {
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            floatAttributes.set(order, value);
        } else {
            attributeOrders.put(attributeId + 1, floatAttributes.size);
            floatAttributes.add(value);
        }
    }

    @Override
    public void setBooleanValue(int attributeId, boolean value) {
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            booleanAttributes.set(order, value);
        } else {
            attributeOrders.put(attributeId + 1, booleanAttributes.size);
            booleanAttributes.add(value);
        }
    }

    @Override
    public void setLongValue(int attributeId, long value) {
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            longAttributes.set(order, value);
        } else {
            attributeOrders.put(attributeId + 1, longAttributes.size);
            longAttributes.add(value);
        }
    }

    @Override
    public void setCharValue(int attributeId, char value) {
        int order = attributeOrders.get(attributeId + 1, -1);
        if (order >= 0) {
            charAttributes.set(order, value);
        } else {
            attributeOrders.put(attributeId + 1, charAttributes.size);
            charAttributes.add(value);
        }
    }

    @Override
    public String toString() {
        return "AttributeHolder [booleanAttributes=" + booleanAttributes + ", intAttributes=" + intAttributes + ", floatAttributes=" + floatAttributes
        + ", charAttributes=" + charAttributes + ", longAttributes=" + longAttributes + ", stringAttributes=" + stringAttributes + "]";
    }


}
