/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

import com.badlogic.gdx.utils.LongArray;
import com.badlogic.gdx.utils.LongMap;
import com.badlogic.gdx.utils.LongMap.Values;

/**
 *  Container for entities that belong to a space
 *
 * @author dpeters
 *
 */
public class EntityMap<T extends Entity> {

    /**
     *   Stores all entities using generic type - not as good for performace as a LongMap to a specific type,
     *   and may require a cast if the method is not generic (such as non-Attributes)
     */
    private LongMap<T> entities;
    private EntityStateMap stateMap;

    public EntityMap(int entityCapacity) {
        entities = new LongMap<T>(entityCapacity);
        stateMap = new EntityStateMap();
    }

    public T removeEntity(long id) {
        if (entities.containsKey(id)) {
            entities.remove(id);
        }

        T entity = entities.get(id);
        stateMap.removeEntity(entity);

        return entity;
    }

    public void addEntity(T entity) {
        entities.put(entity.getId(), entity);
        stateMap.addEntity(entity);
    }

    /**
     *
     * @param entityType
     * @param id
     * @return
     */
    public T lookupEntity(long id) {
        return entities.get(id);
    }

    /**
     *   Returns all of the entities in this space
     *
     * @return
     */
    public Values<T> getAllEntities() {
        return entities.values();
    }

    public LongArray getEntitiesByLifecycleState(EntityLifecycleState lifecycleState) {
        return stateMap.getEntitiesByLifecycleState(lifecycleState);
    }

    public void updateEntityLifecycleState(long entityId, EntityLifecycleState newState) {
        stateMap.updateLifecycleState(entities.get(entityId), newState);
    }

    public void updateEntityState(long entityId, EntityState newState) {
        stateMap.updateState(entities.get(entityId), newState);
    }

    /**
     * Removes all entities
     */
    public void clear() {
        entities.clear();
        stateMap.clear();
    }

}
