/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

public abstract class Model {

    private SpaceMap spaces;

    public Model() {
        spaces = new SpaceMap();
    }

    public abstract SpaceId[] spaces();

    public abstract Space<? extends Entity> createSpace(SpaceId id);

    public void init() {
        for (SpaceId spaceId : spaces()) {
            Space<? extends Entity> space = spaces.getSpace(spaceId);
            if (space == null) {
                //space = spaceFactory.createSpace(spaceId);
                space = createSpace(spaceId);
                spaces.addSpace(space);
            }
            space.reset();
        }
    }

    public void resetSpace(SpaceId spaceId) {
        Space<? extends Entity> space = spaces.getSpace(spaceId);
        space.reset();
    }

    public SpaceMap getAllSpaces() {
        return spaces;
    }

    public Space<? extends Entity> getSpace(SpaceId spaceId) {
        return spaces.getSpace(spaceId);
    }

    public SpaceId getSpaceIdByInt(int intId) {
        for (SpaceId id : spaces()) {
            if (id.getPrimitiveId() == intId) {
                return id;
            }
        }
        return null;
    }

}
