/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model.access;

import com.dodecahelix.libgdx.template.model.access.impl.GridBoundsAccessor;

/**
 *   Contains all the API's that can ONLY read data to the model.
 *
 *   Used to distinguish which API's can be exposed directly to the view
 *
 * @author dpeters
 *
 */
public class ReadOnlyAccessorMap {

    GridBoundsAccessor gridBoundsAccessor;

    public ReadOnlyAccessorMap(GridBoundsAccessor gridBoundsAccessor) {
        super();
        this.gridBoundsAccessor = gridBoundsAccessor;
    }

    public GridBoundsAccessor getGridBoundsAccessor() {
        return gridBoundsAccessor;
    }

}
