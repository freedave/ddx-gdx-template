/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.model;

import com.badlogic.gdx.utils.LongMap;
import com.badlogic.gdx.utils.LongMap.Values;
import com.dodecahelix.libgdx.template.model.chessboard.Chessboard;
import com.dodecahelix.libgdx.template.model.dimless.DimensionlessSpace;
import com.dodecahelix.libgdx.template.model.interfaces.UserInterfaceSpace;
import com.dodecahelix.libgdx.template.model.sidescroll.Sidescroller;
import com.dodecahelix.libgdx.template.model.threedim.Universe;
import com.dodecahelix.libgdx.template.model.tilemap.TileMap;
import com.dodecahelix.libgdx.template.model.twodim.Grid;

/**
 *   Maps spaces by their types, to prevent casting and performance issues
 *
 * @author dpeters
 *
 */
public class SpaceMap {

    // spaces should be one of these types
    private LongMap<Grid> grids;
    private LongMap<Universe> universes;
    private LongMap<Chessboard> chessboards;
    private LongMap<TileMap> tilemaps;
    private LongMap<DimensionlessSpace> dimensionlessSpaces;
    private LongMap<Sidescroller> sidescrollers;
    private LongMap<UserInterfaceSpace> userInterfaceSpaces;

    public SpaceMap() {
        grids = new LongMap<Grid>(0);
        universes = new LongMap<Universe>(0);
        chessboards = new LongMap<Chessboard>(0);
        tilemaps = new LongMap<TileMap>(0);
        sidescrollers = new LongMap<Sidescroller>(0);
        dimensionlessSpaces = new LongMap<DimensionlessSpace>(0);
        userInterfaceSpaces = new LongMap<UserInterfaceSpace>(0);
    }

    /**
     *   This is a more performant method than getSpace(SpaceId)
     *
     * @param spaceId
     * @return
     */
    public Grid getGrid(SpaceId spaceId) {
        return grids.get(spaceId.getPrimitiveId());
    }

    public Universe getUniverse(SpaceId universeId) {
        return universes.get(universeId.getPrimitiveId());
    }

    public Space<? extends Entity> getSpace(SpaceId spaceId) {
        switch (spaceId.getType()) {
            case GRID:
                return grids.get(spaceId.getPrimitiveId());
            case UNIVERSE:
                return universes.get(spaceId.getPrimitiveId());
            case DIMENSIONLESS:
                return dimensionlessSpaces.get(spaceId.getPrimitiveId());
            case SIDESCROLLER:
                return sidescrollers.get(spaceId.getPrimitiveId());
            case TILEMAP:
                return tilemaps.get(spaceId.getPrimitiveId());
            case CHESSBOARD:
                return chessboards.get(spaceId.getPrimitiveId());
            case USER_INTERFACE:
                return userInterfaceSpaces.get(spaceId.getPrimitiveId());
        }

        throw new IllegalArgumentException("Space Type " + spaceId.getType() + "is not supported.");
    }

    public void addSpace(Space<? extends Entity> space) {

        // casts here are fine..  this only gets called once during app initialization
        switch (space.getType()) {
            case GRID:
                grids.put(space.getId().getPrimitiveId(), (Grid) space);
                break;
            case UNIVERSE:
                universes.put(space.getId().getPrimitiveId(), (Universe) space);
                break;
            case DIMENSIONLESS:
                dimensionlessSpaces.put(space.getId().getPrimitiveId(), (DimensionlessSpace) space);
                break;
            case SIDESCROLLER:
                sidescrollers.put(space.getId().getPrimitiveId(), (Sidescroller) space);
                break;
            case TILEMAP:
                tilemaps.put(space.getId().getPrimitiveId(), (TileMap) space);
                break;
            case CHESSBOARD:
                chessboards.put(space.getId().getPrimitiveId(), (Chessboard) space);
                break;
            case USER_INTERFACE:
                userInterfaceSpaces.put(space.getId().getPrimitiveId(), (UserInterfaceSpace) space);
                break;
        }
    }

    public Values<Grid> getGrids() {
        return grids.values();
    }

    public Values<Universe> getUniverses() {
        return universes.values();
    }

    public Values<Chessboard> getChessboardSpaces() {
        return chessboards.values();
    }

    public Values<TileMap> getTilemapSpaces() {
        return tilemaps.values();
    }

    public Values<Sidescroller> getSidescrollerSpaces() {
        return sidescrollers.values();
    }

    public Values<UserInterfaceSpace> getUserInterfaceSpaces() {
        return userInterfaceSpaces.values();
    }

    public Values<DimensionlessSpace> getDimensionlessSpaces() {
        return dimensionlessSpaces.values();
    }

}
