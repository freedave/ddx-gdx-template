/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim.collision;

import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.Space;
import com.dodecahelix.libgdx.template.model.attributes.AttributeHolder;
import com.dodecahelix.libgdx.template.types.Attribute.StandardAttribute;

/**
 *   Update the model as a result of the collision
 *
 * @author dpeters
 *
 */
public class CollisionExecution {

    public CollisionExecution() {
    }

    public void doCollision(Space<? extends Entity> space, CollisionResolution resolution) {
        CollisionCircle source = resolution.getSourceCircle();
        CollisionCircle target = resolution.getTargetCircle();

        doBounce(source, space.lookupEntity(source.getCollisionId()));
        doBounce(target, space.lookupEntity(target.getCollisionId()));
    }

    private void doBounce(CollisionCircle circle, Entity entity) {
        AttributeHolder attributes = entity.getAttributes();
        attributes.setFloatValue(StandardAttribute.MOVEMENT_ANGLE.getId(), circle.getRotation());
        attributes.setFloatValue(StandardAttribute.SPEED.getId(), circle.getSpeed());
    }

}
