/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim;

public class Circle {

    private MovablePoint center;
    private float radius;

    public Circle(float x, float y, float radius) {
        center = new MovablePoint(x, y);
        this.radius = radius;
    }

    public Circle(MovablePoint center, float radius) {
        this.center = center;
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public float getRadius() {
        return radius;
    }

    public void setCenterX(float x) {
        center.setX(x);
    }

    public void setCenterY(float y) {
        center.setY(y);
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

}
