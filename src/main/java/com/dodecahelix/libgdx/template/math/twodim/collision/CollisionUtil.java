/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim.collision;

import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.EntityState;

public class CollisionUtil {

    /**
     *  Check to see if the source asteroid is colliding with the target asteroid
     *
     * @param target
     * @return
     */
    public static boolean checkCollision(CollisionCircle target, CollisionCircle source) {

        boolean collision = false;

        float xDist = source.getCenter().getX() - target.getCenter().getX();
        float yDist = source.getCenter().getY() - target.getCenter().getY();

        // calculation used ;
        // http://www.gamasutra.com/view/feature/3015/pool_hall_lessons_fast_accurate_.php
        if ((Math.pow(xDist, 2) + Math.pow(yDist, 2)) <= Math.pow(target.getRadius() + source.getRadius(), 2)) {
            collision = true;
        }

        return collision;
    }

    /**
     *   Determines if an entity is collidable
     *
     * @param entity
     * @return
     */
    public static boolean isCollidable(Entity entity) {
        boolean collidable = false;

        // if its not a collidable object, its state shoudl be background or phantom
        if ((EntityState.ACTIVE == entity.getState()) || (EntityState.INVISIBLE == entity.getState())) {
            collidable = true;
        }

        // determine collidability based on entity type
        /*
		if (CommonEntity.BALL.getId() == entity.getEntityType()) {
			return true;
		}
		*/

        return collidable;
    }

    public static float getCollisionMidpointX(CollisionCircle sourceCircle, CollisionCircle targetCircle) {
        return ((sourceCircle.getCenter().getX() * targetCircle.getRadius()) + (targetCircle.getCenter().getX() * sourceCircle.getRadius())) / (sourceCircle.getRadius() + targetCircle.getRadius());
    }

    public static float getCollisionMidpointY(CollisionCircle sourceCircle, CollisionCircle targetCircle) {
        return ((sourceCircle.getCenter().getY() * targetCircle.getRadius()) + (targetCircle.getCenter().getY() * sourceCircle.getRadius())) / (sourceCircle.getRadius() + targetCircle.getRadius());
    }

}
