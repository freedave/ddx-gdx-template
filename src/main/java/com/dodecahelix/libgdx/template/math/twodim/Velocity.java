/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim;

public class Velocity {

    // velocity along the X axis
    private double velocityX;

    // velocity along the Y axis
    private double velocityY;

    /**
     *   Calculates the x and y components of velocity, given a speed and angle in the Grid system (where 0 degrees is north)
     *
     * @param angle
     * @param speed
     */
    public Velocity(float angle, float speed) {
        setVelocity(angle, speed);
    }

    public void setVelocity(float angle, float speed) {
        double xAxisAngle = Math.toRadians(90 - angle);
        velocityX = speed * Math.cos(xAxisAngle);
        velocityY = speed * Math.sin(xAxisAngle);
    }

    public double getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(double velocityX) {
        this.velocityX = velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(double velocityY) {
        this.velocityY = velocityY;
    }

    public float getSpeed() {
        float speed = (float) (Math.sqrt(Math.pow(velocityX, 2) + Math.pow(velocityY, 2)));
        return speed;
    }

    public float getRotation() {
        // use atan2 or you lose quadrant information
        return 90f - (float) Math.toDegrees(Math.atan2(velocityY, velocityX));
    }

    @Override
    public String toString() {
        return "Velocity [velocityX=" + velocityX + ", velocityY=" + velocityY + "]";
    }

}
