/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim;

/**
 *   Bounds are fixed position rectangular areas
 *
 * @author dpeters
 *
 */
public class Bounds {

    private FixedPoint bottomLeftBound;
    private FixedPoint topRightBound;

    // Calculated on the fly when requested, as width and height may change.  Cache this object to prevent garbage collection
    private FixedPoint centerpoint;

    public Bounds(float width, float height) {
        bottomLeftBound = new FixedPoint(0, 0);
        topRightBound = new FixedPoint(width, height);

        calculateCenterpoint();
    }

    public Bounds(FixedPoint bottomLeft, FixedPoint topRight) {
        bottomLeftBound = bottomLeft;
        topRightBound = topRight;

        calculateCenterpoint();
    }

    private void calculateCenterpoint() {
        float centerX = bottomLeftBound.getX() + (getWidth() / 2f);
        float centerY = bottomLeftBound.getY() + (getHeight() / 2f);

        centerpoint = new FixedPoint(centerX, centerY);
    }

    public FixedPoint getCenterpoint() {
        return centerpoint;
    }

    public boolean containsPoint(float x, float y) {
        if (x > topRightBound.getX()) {
            return false;
        }
        if (x < bottomLeftBound.getX()) {
            return false;
        }
        if (y > topRightBound.getY()) {
            return false;
        }
        if (y < bottomLeftBound.getY()) {
            return false;
        }
        return true;
    }

    public float getWidth() {
        return (topRightBound.getX() - bottomLeftBound.getX());
    }

    public float getHeight() {
        return (topRightBound.getY() - bottomLeftBound.getY());
    }

    public float getMaxX() {
        return topRightBound.getX();
    }

    public float getMaxY() {
        return topRightBound.getY();
    }

    public float getMinX() {
        return bottomLeftBound.getX();
    }

    public float getMinY() {
        return bottomLeftBound.getY();
    }

    @Override
    public String toString() {
        return "Bounds [getCenterpoint()=" + getCenterpoint() + ", getWidth()="
        + getWidth() + ", getHeight()=" + getHeight() + ", getMaxX()="
        + getMaxX() + ", getMaxY()=" + getMaxY() + ", getMinX()="
        + getMinX() + ", getMinY()=" + getMinY() + "]";
    }


}
