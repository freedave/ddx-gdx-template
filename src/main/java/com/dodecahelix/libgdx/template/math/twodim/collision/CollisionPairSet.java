/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim.collision;

import com.badlogic.gdx.utils.LongArray;

/**
 *   Maintains a set of object-id pairings, making it easy to test if the set contains the pair (in any order)
 *
 * @author dpeters
 *
 */
public class CollisionPairSet {

    // Use an array of longs to prevent the need for garbage collection each frame
    private LongArray sourceLongs;
    private LongArray targetLongs;

    public CollisionPairSet(int arraySize) {
        sourceLongs = new LongArray(arraySize);
        targetLongs = new LongArray(arraySize);
    }

    public void addCollisionPair(long sourceId, long targetId) {
        sourceLongs.add(sourceId);
        targetLongs.add(targetId);
    }

    /**
     *   Checks to see if these two objects are paired in this set (regardless of source/target order)
     *
     * @param sourceId
     * @param targetId
     * @return
     */
    public boolean containsPair(long sourceId, long targetId) {
        boolean contains = false;

        for (int i = 0; i < sourceLongs.size; i++) {
            if (sourceLongs.get(i) == sourceId) {
                if (targetLongs.get(i) == targetId) {
                    contains = true;
                }
            }
            if (sourceLongs.get(i) == targetId) {
                if (targetLongs.get(i) == sourceId) {
                    contains = true;
                }
            }
        }
        return contains;
    }

    public void clear() {
        sourceLongs.clear();
        targetLongs.clear();
    }

    public void addAll(CollisionPairSet otherCollisionPairs) {
        sourceLongs.addAll(otherCollisionPairs.getSourceLongs());
        targetLongs.addAll(otherCollisionPairs.getTargetLongs());
    }

    public LongArray getSourceLongs() {
        return sourceLongs;
    }

    public LongArray getTargetLongs() {
        return targetLongs;
    }


}
