/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim;


/**
 *
 *   When a point hits the limits of a bound, set the new point position on the opposite side of the bounds
 *
 * @author dpeters
 *
 */
public class BoundaryWrapper {

    /**
     *
     * When a position exceeds the boundary limits of the grid,
     *     wrap the position to the other side of the grid
     *
     * @param position
     * @param boundaries
     * @param paddingX - distance from the edge (in X dir) where you begin to wrap
     * @param paddingY - distance from the edge (in Y dir) where you begin to wrap
     * @return
     */
    public static MovablePoint wrapPosition(MovablePoint position, Bounds boundaries) {

        float x = position.getX();
        float y = position.getY();
        position.setX(wrapPositionX(x, boundaries));
        position.setY(wrapPositionY(y, boundaries));

        return position;
    }

    public static float wrapPositionX(float x, Bounds boundaries) {
        float boundaryMaxX = boundaries.getMaxX();
        float boundaryMinX = boundaries.getMinX();

        // at boundary, flip over to other side
        if (x < boundaryMinX) {
            x = boundaryMaxX - (boundaryMinX - x);
        }
        if (x > boundaryMaxX) {
            x = boundaryMinX + (x - boundaryMaxX);
        }

        return x;
    }

    public static float wrapPositionY(float y, Bounds boundaries) {
        float boundaryMaxY = boundaries.getMaxY();
        float boundaryMinY = boundaries.getMinY();

        // at boundary, flip over to other side
        if (y < boundaryMinY) {
            y = boundaryMaxY - (boundaryMinY - y);
        }
        if (y > boundaryMaxY) {
            y = boundaryMinY + (y - boundaryMaxY);
        }

        return y;
    }

}
