/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim.collision;

import com.dodecahelix.libgdx.template.math.twodim.Circle;

/**
 *   Describes an entity in a collision state, including pre and post collision status
 *
 * @author dpeters
 *
 */
public class CollisionCircle extends Circle {

    /*
     *  magnitude of velocity vector
     */
    private float speed;

    /*
     *  angle of velocity vector from the y axis (i.e; 0 = north, 90 = east, etc..)
     */
    private float rotation;

    private float mass;

    /*
     *  collision object id of the object that this represents (for distinguishing two different circles)
     */
    private long collisionObjectId;

    /**
     *  Keep track of the original state of the circle - speed and rotation will change
     */
    private float originalRotation;
    private float originalSpeed;

    /**
     *  Measure of the energy exhausted in the collision
     */
    private double impact;

    public CollisionCircle(long objectId, float x, float y, float radius, float speed, float rotation, float mass) {
        super(x, y, radius);

        this.collisionObjectId = objectId;
        this.mass = mass;
        this.rotation = rotation;
        this.speed = speed;

        this.originalRotation = rotation;
        this.originalSpeed = speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getSpeed() {
        return speed;
    }

    public float getRotation() {
        return rotation;
    }

    public float getMass() {
        return mass;
    }

    public long getCollisionId() {
        return collisionObjectId;
    }

    public float getOriginalRotation() {
        return originalRotation;
    }

    public float getOriginalSpeed() {
        return originalSpeed;
    }

    public double getImpact() {
        return impact;
    }

    public void setImpact(double impact) {
        this.impact = impact;
    }

    public long getCollisionObjectId() {
        return collisionObjectId;
    }

    public void setCollisionObjectId(long collisionObjectId) {
        this.collisionObjectId = collisionObjectId;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }

    public void setOriginalRotation(float originalRotation) {
        this.originalRotation = originalRotation;
    }

    public void setOriginalSpeed(float originalSpeed) {
        this.originalSpeed = originalSpeed;
    }

    @Override
    public String toString() {
        return "CollisionCircle [speed=" + speed + ", rotation=" + rotation
        + ", mass=" + mass + ", collisionObjectId=" + collisionObjectId
        + ", originalRotation=" + originalRotation + ", originalSpeed="
        + originalSpeed + ", impact=" + impact + "]";
    }


}
