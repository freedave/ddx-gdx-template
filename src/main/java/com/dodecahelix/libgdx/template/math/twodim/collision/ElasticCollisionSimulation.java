/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim.collision;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.math.twodim.Velocity;
import com.dodecahelix.libgdx.template.math.twodim.VelocityUtil;

/**
 *   Given two collidable objects (defined by a collision circle), find the result of an elastic collision between these two
 *
 * @author dpeters
 *
 */
public class ElasticCollisionSimulation implements CollisionSimulation {

    private CollisionCircle alphaCircle;
    private CollisionCircle betaCircle;

    // Pool velocity, used in collisions to prevent garbage collection for each collision
    private Velocity alphaVelocity;
    private Velocity betaVelocity;

    // coefficient of restitution (0 = inelastic, 1 = elastic)
    private float restitution;

    private boolean debugCollisions;

    public ElasticCollisionSimulation(Properties properties) {
        this.restitution = properties.getFloatProperty(PropertyName.COLLISION_RESTITUTION.name());

        // create instance-level velocity vectors to prevent instantiation and garbage collection
        alphaVelocity = new Velocity(0, 1);
        betaVelocity = new Velocity(0, 1);

        debugCollisions = properties.getBooleanProperty(PropertyName.LOG_COLLISIONS.name());
    }

    @Override
    public synchronized CollisionResolution simulateCollision(CollisionCircle source, CollisionCircle target) {

        // the alpha circle is the one furthest to the left
        boolean alphaSource = true;

        // TODO - do we need this logic any more?
        if (source.getCenter().getX() > target.getCenter().getX()) {
            alphaSource = false;
            alphaCircle = target;
            betaCircle = source;
        } else {
            // alpha is source
            alphaCircle = source;
            betaCircle = target;
        }

        if (debugCollisions) {
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver:: alpha circle = " + alphaCircle);
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver:: beta circle = " + betaCircle);
        }

        resolveCollisionUsingTranslation();

        alphaCircle.setImpact(calculateImpact(alphaCircle));
        betaCircle.setImpact(calculateImpact(betaCircle));

        // garbage only produced on a collision, so object creation should be ok
        CollisionResolution resolution = null;
        if (alphaSource) {
            resolution = new CollisionResolution(alphaCircle, betaCircle);
        } else {
            resolution = new CollisionResolution(betaCircle, alphaCircle);
        }

        return resolution;
    }

    /**
     *   TODO - Does this make sense??
     *
     * @param circle
     * @return
     */
    private double calculateImpact(CollisionCircle circle) {

        float originalRotation = circle.getOriginalRotation();
        float originalSpeed = circle.getOriginalSpeed();
        float rotation = circle.getRotation();
        float speed = circle.getSpeed();

        double xAxisAngle = Math.toRadians(90 - originalRotation);
        double ovx = originalSpeed * Math.cos(xAxisAngle);
        double ovy = originalSpeed * Math.sin(xAxisAngle);

        xAxisAngle = Math.toRadians(90 - rotation);
        double fvx = speed * Math.cos(xAxisAngle);
        double fvy = speed * Math.sin(xAxisAngle);

        double vxDelta = (ovx - fvx);
        double vyDelta = (ovy - fvy);
        return Math.sqrt(Math.pow(vxDelta, 2) + Math.pow(vyDelta, 2));
    }

    private void resolveCollisionUsingTranslation() {
        //alphaVelocity = new Velocity(alphaCircle.getRotation(), alphaCircle.getSpeed());
        //betaVelocity = new Velocity(betaCircle.getRotation(), betaCircle.getSpeed());
        alphaVelocity.setVelocity(alphaCircle.getRotation(), alphaCircle.getSpeed());
        betaVelocity.setVelocity(betaCircle.getRotation(), betaCircle.getSpeed());

        if (debugCollisions) {
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver::resolveCollisionUsingTranslation() alpha velocity - " + alphaVelocity);
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver::resolveCollisionUsingTranslation() beta velocity - " + betaVelocity);
        }
        double normAngle = Math.atan((betaCircle.getCenter().getY() - alphaCircle.getCenter().getY()) / (betaCircle.getCenter().getX() - alphaCircle.getCenter().getX()));

        //double normDegrees = Math.toDegrees(normAngle);
        //LOGGER.debug("CollisionResolver::resolveCollisionUsingTranslation() normAngle = " + normDegrees);

        alphaVelocity = VelocityUtil.translate(alphaVelocity, normAngle);
        betaVelocity = VelocityUtil.translate(betaVelocity, normAngle);

        if (debugCollisions) {
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver::resolveCollisionUsingTranslation() norm alpha velocity - " + alphaVelocity);
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver::resolveCollisionUsingTranslation() norm beta velocity - " + betaVelocity);
        }

        double alphaFinalVelocityCollision = restitution * ((alphaVelocity.getVelocityX() * (alphaCircle.getMass() - betaCircle.getMass())) + 2 * betaCircle.getMass() * betaVelocity.getVelocityX()) / (alphaCircle.getMass() + betaCircle.getMass());
        double betaFinalVelocityCollision = restitution * ((betaVelocity.getVelocityX() * (betaCircle.getMass() - alphaCircle.getMass())) + 2 * alphaCircle.getMass() * alphaVelocity.getVelocityX()) / (alphaCircle.getMass() + betaCircle.getMass());

        if (debugCollisions) {
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver:: alpha final collision velocity = " + alphaFinalVelocityCollision);
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver:: beta final collision velocity = " + betaFinalVelocityCollision);
        }

        alphaVelocity.setVelocityX(alphaFinalVelocityCollision);
        betaVelocity.setVelocityX(betaFinalVelocityCollision);

        // now, rotate back to original reference frame
        alphaVelocity = VelocityUtil.translate(alphaVelocity, -normAngle);
        betaVelocity = VelocityUtil.translate(betaVelocity, -normAngle);

        if (debugCollisions) {
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver:: alpha velocity " + alphaVelocity + ", newRotation = " + alphaVelocity.getRotation() + ", newSpeed = " + alphaVelocity.getSpeed());
            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "CollisionResolver:: beta velocity " + betaVelocity + ", newRotation = " + betaVelocity.getRotation() + ", newSpeed = " + betaVelocity.getSpeed());
        }

        // now, update the roids
        alphaCircle.setSpeed((float) alphaVelocity.getSpeed());
        alphaCircle.setRotation((float) alphaVelocity.getRotation());
        betaCircle.setSpeed((float) betaVelocity.getSpeed());
        betaCircle.setRotation((float) betaVelocity.getRotation());
    }

}
