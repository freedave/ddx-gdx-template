/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim;

public class MotionUtil {

    /**
     *   Given a moving entity's rotation angle and speed, find its new X position based on how much time has passed
     * @param xPos
     * @param rotationAngle - angle that the entity is moving, clockwise from due north
     * @param speed - units of measure per second
     * @param delta - how much time has passed (in secs)
     * @return
     */
    public static float updatePositionXDir(float xPos, float rotationAngle, float speed, float delta) {
        // theta is the CCW angle from the positive X axis (east) - rotation angle is CW from due north
        float theta = 90 - rotationAngle;

        // ?? is there a performance hit by truncating to float
        xPos = (float) (xPos + (speed * delta) * Math.cos(Math.toRadians(theta)));
        return xPos;
    }

    public static float updatePositionYDir(float yPos, float rotationAngle, float speed, float delta) {
        // theta is the CCW angle from the positive X axis (east)
        float theta = 90 - rotationAngle;

        // ?? is there a performance hit by truncating to float
        yPos = (float) (yPos + (speed * delta) * Math.sin(Math.toRadians(theta)));
        return yPos;
    }

}
