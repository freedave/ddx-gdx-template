/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim.collision;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.EntityState;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.SpaceState;
import com.dodecahelix.libgdx.template.model.attributes.AttributeHolder;
import com.dodecahelix.libgdx.template.model.twodim.Grid;
import com.dodecahelix.libgdx.template.types.Attribute.StandardAttribute;

/**
 *   Handles elastic collisions in the model, for 2d spaces
 *
 */
public class EntityCollisionAccessor {

    Model model;

    /**
     *   Independent math utility that simulates a collision between two circles
     */
    private CollisionSimulation collisionSimulation;
    private CollisionExecution collisionExecution;

    /*
     *  Cache the list of collision entities for collection re-use
     */
    private List<Entity> collidables;

    // cache for re-use during collision loop
    private CollisionCircle sourceCircle;
    private CollisionCircle targetCircle;
    private CollisionResolution resolution;

    /*
     *  once a collision occurs, the two colliding objects (by ID) are placed in this list until they are clear of one another
     */
    private CollisionPairSet thisCycleCollisionPairs;

    /*
     *  the pairs from the previous frame.  If the pairs are already in collision state, don't collide again until they are apart
     */
    private CollisionPairSet lastCycleCollisionPairs;

    private boolean debugCollisions;

    public EntityCollisionAccessor(Model model, Properties properties) {
        this.model = model;

        int entityCapacity = properties.getIntProperty(PropertyName.ENTITY_CAPACITY.name());
        debugCollisions = properties.getBooleanProperty(PropertyName.LOG_COLLISIONS.name());

        collisionSimulation = new ElasticCollisionSimulation(properties);
        collisionExecution = new CollisionExecution();

        collidables = new ArrayList<Entity>(entityCapacity / 10);
        thisCycleCollisionPairs = new CollisionPairSet(entityCapacity);
        lastCycleCollisionPairs = new CollisionPairSet(entityCapacity);

        // create holders for the circle value objects so we aren't creating a new CollsionCircle for each comparison
        sourceCircle = new CollisionCircle(0, 0, 0, 0, 0, 0, 0);
        targetCircle = new CollisionCircle(0, 0, 0, 0, 0, 0, 0);
    }

    /**
     *   In most cases, we just want to use the grid viewport to resolve all collisions
     */
    public void resolveAllActiveGridCollisionsByViewport() {

        // collisions only apply to Grid's
        for (Grid grid : model.getAllSpaces().getGrids()) {
            if (SpaceState.ACTIVE == grid.getState()) {
                resolveCollisions(grid.getId(), grid.getViewportBounds());
            }
        }
    }

    /**
     *   For a given space, resolve all entity collisions in the given bounds of the space,
     *     updating the velocity and rotations of the given entity
     *
     *   (Bounded for performance.  Only calculate collisions that occurs on-screen, for example)
     *
     * @param space
     * @param bounds - Bounds most likely should resolve to the viewport of the grid (or slightly over the bounds)
     */
    public synchronized void resolveCollisions(SpaceId space, Bounds bounds) {

        collidables = fetchPotentialCollisions(space, bounds);

        // keep track of which objects are in the collision state in this frame
        thisCycleCollisionPairs.clear();

        for (int i = 0; i < collidables.size(); i++) {
            Entity sourceCollidable = collidables.get(i);
            for (int j = i + 1; j < collidables.size(); j++) {
                Entity targetCollidable = collidables.get(j);

                sourceCircle = newCollisionCircleFromEntity(sourceCircle, sourceCollidable);
                targetCircle = newCollisionCircleFromEntity(targetCircle, targetCollidable);
                if (CollisionUtil.checkCollision(targetCircle, sourceCircle)) {

                    long sourceId = sourceCollidable.getId();
                    long targetId = targetCollidable.getId();

                    thisCycleCollisionPairs.addCollisionPair(sourceId, targetId);

                    // check to see that these two haven't already gone through the collision
                    if (!lastCycleCollisionPairs.containsPair(sourceId, targetId)) {
                        if (debugCollisions) {
                            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "Simulating collision between two entities : " + sourceId + ", " + targetId);
                        }
                        // compute the effect of these two colliding entities
                        resolution = collisionSimulation.simulateCollision(sourceCircle, targetCircle);

                        if (debugCollisions) {
                            Gdx.app.debug(SharedConstants.LOGGING_TAG_COLLISIONS, "Collision resolution is " + resolution);
                        }

                        collisionExecution.doCollision(model.getSpace(space), resolution);
                    }
                }
            }
        }

        // update collision states
        lastCycleCollisionPairs.clear();
        lastCycleCollisionPairs.addAll(thisCycleCollisionPairs);
    }

    /**
     *   Find all entities in the given bounds of the given space that have the potential to collide with one another
     *
     * @param space
     * @param bounds
     */
    private List<Entity> fetchPotentialCollisions(SpaceId space, Bounds bounds) {

        collidables.clear();
        for (Entity entity : model.getSpace(space).getAllEntities()) {

            // check that the entity is a collidable type, is active (or phantom), and within the given bounds
            if (CollisionUtil.isCollidable(entity)
            && (EntityState.ACTIVE == entity.getState() || EntityState.INVISIBLE == entity.getState())
            && bounds.containsPoint(entity.getAttributes().getFloatValue(StandardAttribute.POSITION_X.getId(), -1),
            entity.getAttributes().getFloatValue(StandardAttribute.POSITION_Y.getId(), -1))) {

                collidables.add(entity);
            }
        }

        return collidables;

    }

    /**
     *   Reuse the CollisionCircle object using entity attributes
     *
     * @param circle
     * @param entity
     * @return
     */
    private CollisionCircle newCollisionCircleFromEntity(CollisionCircle circle, Entity entity) {
        AttributeHolder attributes = entity.getAttributes();
        circle.setSpeed(attributes.getFloatValue(StandardAttribute.SPEED.getId(), 0));
        circle.setRotation(attributes.getFloatValue(StandardAttribute.MOVEMENT_ANGLE.getId(), 0));
        circle.setMass(attributes.getFloatValue(StandardAttribute.MASS.getId(), 0));
        circle.setOriginalRotation(attributes.getFloatValue(StandardAttribute.MOVEMENT_ANGLE.getId(), 0));
        circle.setOriginalSpeed(attributes.getFloatValue(StandardAttribute.SPEED.getId(), 0));
        circle.setCenterX(attributes.getFloatValue(StandardAttribute.POSITION_X.getId(), 0));
        circle.setCenterY(attributes.getFloatValue(StandardAttribute.POSITION_Y.getId(), 0));
        circle.setRadius(attributes.getFloatValue(StandardAttribute.SIZE.getId(), 0) / 2);
        circle.setCollisionObjectId(entity.getId());
        circle.setImpact(0);

        return circle;
    }


}
