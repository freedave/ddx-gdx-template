/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.math.twodim.collision;


/**
 *    Displays the end result of the collision of two circles
 *
 * @author dpeters
 *
 */
public class CollisionResolution {

    private CollisionCircle sourceCircle;
    private CollisionCircle targetCircle;

    public CollisionResolution(CollisionCircle sourceCircle, CollisionCircle targetCircle) {
        super();

        this.sourceCircle = sourceCircle;
        this.targetCircle = targetCircle;
    }

    public CollisionCircle getSourceCircle() {
        return sourceCircle;
    }

    public void setSourceCircle(CollisionCircle sourceCircle) {
        this.sourceCircle = sourceCircle;
    }

    public CollisionCircle getTargetCircle() {
        return targetCircle;
    }

    public void setTargetCircle(CollisionCircle targetCircle) {
        this.targetCircle = targetCircle;
    }

    @Override
    public String toString() {
        return "CollisionResolution [sourceCircle=" + sourceCircle
        + ", targetCircle=" + targetCircle + "]";
    }

}
