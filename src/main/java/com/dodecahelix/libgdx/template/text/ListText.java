/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.text;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.libgdx.template.ui.RgbColor;

public class ListText implements TextHolder {

    private TextStyleAlign defaultAlign;
    private TextStyleSize defaultSize;
    private RgbColor defaultColor;

    private RgbColor hyperlinkColor = RgbColor.BLUE_MIDNIGHTBLUE;

    private List<TextLine> lines;

    public ListText(TextStyleAlign defaultAlign, TextStyleSize defaultSize, RgbColor defaultColor) {
        lines = new ArrayList<TextLine>();
        this.defaultAlign = defaultAlign;
        this.defaultSize = defaultSize;
        this.defaultColor = defaultColor;
    }

    public void addLine(int indent, String text, String hyperlink,
                        TextStyleDecor decor, TextStyleAlign align, TextStyleSize size, RgbColor color) {

        TextLine line = new TextLine(text, decor, align, size, color);
        line.setIndent(indent);
        line.setHyperlink(hyperlink);
        lines.add(line);
    }

    public void addLine(String text) {
        addLine(0, text, null, null, defaultAlign, defaultSize, defaultColor);
    }

    public void addLine(int indent, String text) {
        addLine(indent, text, null, null, defaultAlign, defaultSize, defaultColor);
    }

    public void addHyperlink(int indent, String text, String hyperlink) {
        addLine(indent, text, hyperlink, null, defaultAlign, defaultSize, hyperlinkColor);
    }

    public void addSpecialLine(String text, TextStyleDecor decor, TextStyleSize size, RgbColor color) {
        lines.add(new TextLine(text, decor, defaultAlign, size, color));
    }

    public void addBreak() {
        lines.add(new TextLine("_", TextStyleDecor.INVISIBLE, defaultAlign, defaultSize, defaultColor));
    }

    public List<TextLine> getLines() {
        return lines;
    }

    public TextStyleAlign getDefaultAlign() {
        return defaultAlign;
    }

    public void setHyperlinkColor(RgbColor hyperlinkColor) {
        this.hyperlinkColor = hyperlinkColor;
    }

}
