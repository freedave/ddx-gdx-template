/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.libgdx.template.text;

import com.dodecahelix.libgdx.template.ui.RgbColor;

public class TextLine {

    private String text;

    /**
     *  this line will forward to a URL if clicked
     */
    private String hyperlink;

    /**
     *  number of units to indent by
     */
    private int indentSize = 0;

    /**
     * A 'pause' line is a flag to indicate that the processing of lines should wait on user input
     */
    private boolean pause;

    private TextStyleDecor decor;
    private TextStyleAlign align;
    private TextStyleSize size;
    private RgbColor color;

    public TextLine(String text,
                    TextStyleDecor decor,
                    TextStyleAlign align,
                    TextStyleSize size,
                    RgbColor color) {

        // defaults
        if (align == null) {
            align = TextStyleAlign.LEFT;
        }
        if (size == null) {
            size = TextStyleSize.STANDARD;
        }
        if (color == null) {
            color = RgbColor.BLACK;
        }

        this.text = text;
        this.decor = decor;
        this.align = align;
        this.size = size;
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TextStyleDecor getDecor() {
        return decor;
    }

    public void setDecor(TextStyleDecor decor) {
        this.decor = decor;
    }

    public TextStyleAlign getAlign() {
        return align;
    }

    public void setAlign(TextStyleAlign align) {
        this.align = align;
    }

    public TextStyleSize getSize() {
        return size;
    }

    public void setSize(TextStyleSize size) {
        this.size = size;
    }

    public RgbColor getColor() {
        return color;
    }

    public void setColor(RgbColor color) {
        this.color = color;
    }

    public String getHyperlink() {
        return hyperlink;
    }

    public void setHyperlink(String hyperlink) {
        this.hyperlink = hyperlink;
    }

    public int indentSize() {
        return indentSize;
    }

    public void setIndent(int indent) {
        this.indentSize = indent;
    }

    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }
}
