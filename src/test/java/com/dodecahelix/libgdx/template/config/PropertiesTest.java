package com.dodecahelix.libgdx.template.config;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class PropertiesTest {

    @Test
    public void testOverwritePropertiesWithPreferences() {
        Properties properties = new Properties();
        properties.addIntegerProperty("FOO", 10);
        properties.addScalableIntegerProperty("MY_FONT_SIZE", 12);
        
        UserPreferences preferences = new UserPreferences();
        Map<String, Integer> intPreferences = new HashMap<String, Integer>();
        intPreferences.put("FOO", 15);
        Map<String, Integer> scaledIntPreferences = new HashMap<String, Integer>();
        scaledIntPreferences.put("MY_FONT_SIZE", 18);
        
        preferences.setIntegerPreferences(intPreferences);
        preferences.setScalableIntegerPreferences(scaledIntPreferences);
        
        PropertiesUtil.overwriteWithUserPreferences(properties, preferences);
        
        assertEquals(15, properties.getIntProperty("FOO"));
        
        // this is the unscaled property and will be stored in regular int properties
        assertEquals(18, properties.getScaledIntProperty("MY_FONT_SIZE"));
    }

}
