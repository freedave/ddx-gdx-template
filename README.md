# GDX-Template #

### What is this repository for? ###

* GDX-template is a custom MVC framework for use in LibGDX mobile applications, optimized to reduce garbage generated in the game loop.
* It is currently in an experimental and mostly undocumented state.  Expect significant changes in future releases.
* Version: 0.5.0-SNAPSHOT

### How do I get set up? ###

* This is a java library based on JRE 1.7+
* Maven 3+ is required to build
* Run `mvn clean install` on the root directory to generate the library and install to your local repository. 
* Dependencies
    * LibGDX (2D java graphics engine) and GDX-freetype (fonts)s

### Contribution guidelines ###

* If you are interested in contributing to this codebase, please contact freedavep@gmail.com

### License ###

* Licensed under the Apache v2.0 License

### Who can I contact concerning this code? ###

* Contact : freedavep@gmail.com
